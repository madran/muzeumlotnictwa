{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.66 Exporter",
		"vertices"      : 4,
		"faces"         : 1,
		"normals"       : 1,
		"colors"        : 2,
		"uvs"           : [],
		"materials"     : 1,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "projektor_ekran",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorDiffuse" : [0.800000011920929, 0.800000011920929, 0.800000011920929],
		"colorSpecular" : [0.259398490190506, 0.259398490190506, 0.259398490190506],
		"depthTest" : true,
		"depthWrite" : true,
		"shading" : "Lambert",
		"specularCoef" : 29,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	}],

	"vertices" : [-0.00709318,-1.19815,-0.827274,0.00709308,1.19815,-0.827274,-0.00709308,-1.19815,0.827274,0.00709318,1.19815,0.827274],

	"morphTargets" : [],

	"normals" : [-0.999969,0.00589,0],

	"colors" : [16712209,16711695],

	"uvs" : [],

	"faces" : [163,1,0,2,3,0,0,0,0,0,0,1,1,1],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

	"animation" : {}


}
