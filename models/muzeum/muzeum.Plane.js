{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.66 Exporter",
		"vertices"      : 4,
		"faces"         : 1,
		"normals"       : 1,
		"colors"        : 0,
		"uvs"           : [],
		"materials"     : 1,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "default",
		"vertexColors" : false
	}],

	"vertices" : [0.299609,-0.145589,-0.122667,-0.299609,-0.145589,-0.122667,0.299609,0.145589,0.122667,-0.299609,0.145589,0.122667],

	"morphTargets" : [],

	"normals" : [0,-0.644337,0.764733],

	"colors" : [],

	"uvs" : [],

	"faces" : [35,1,0,2,3,0,0,0,0,0],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

	"animation" : {}


}
