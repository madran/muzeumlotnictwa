{

"metadata" :
{
	"formatVersion" : 3.2,
	"type"          : "scene",
	"sourceFile"    : "muzeum_031.blend",
	"generatedBy"   : "Blender 2.66 Exporter",
	"objects"       : 75,
	"geometries"    : 75,
	"materials"     : 35,
	"textures"      : 21
},

"urlBaseType" : "relativeToScene",


"objects" :
{
	"sufit" : {
		"geometry"  : "geo_muzeum.001",
		"groups"    : [  ],
		"material"  : "sufit",
		"position"  : [ -260.979, -54.1475, -258.452 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : true
	},

	"projektor_mysliwce.001" : {
		"geometry"  : "geo_projektor.011",
		"groups"    : [  ],
		"material"  : "czarny",
		"position"  : [ -102.938, -50.346, -273.849 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_bombowce_czarne" : {
		"geometry"  : "geo_projektor.010",
		"groups"    : [  ],
		"material"  : "czarny",
		"position"  : [ 336.964, -50.346, 505.132 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_bombowce_metal" : {
		"geometry"  : "geo_projektor.008",
		"groups"    : [  ],
		"material"  : "srebro",
		"position"  : [ 336.964, -50.346, 505.132 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_bombowce_tasma" : {
		"geometry"  : "geo_projektor.006",
		"groups"    : [  ],
		"material"  : "tasma",
		"position"  : [ 336.964, -50.346, 505.132 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_bombowce_rolka_dol_tasma" : {
		"geometry"  : "geo_rolka_dol.002",
		"groups"    : [  ],
		"material"  : "tasma",
		"position"  : [ 339.858, -54.98, 506.109 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_bombowce_rolka_gora_tasma" : {
		"geometry"  : "geo_rolka_gora.005",
		"groups"    : [  ],
		"material"  : "tasma",
		"position"  : [ 339.892, -44.843, 506.109 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"ekran_statyw_bombowce_rury" : {
		"geometry"  : "geo_ekran_statyw_bombowce.000",
		"groups"    : [  ],
		"material"  : "srebro",
		"position"  : [ 415.897, -41.5426, 505.5 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"projektor_mysliwce_rolka_dol_tasma" : {
		"geometry"  : "geo_rolka_gora.004",
		"groups"    : [  ],
		"material"  : "tasma",
		"position"  : [ -101.961, -54.9799, -276.743 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_mysliwce_rolka_gora_tasma" : {
		"geometry"  : "geo_rolka_gora.003",
		"groups"    : [  ],
		"material"  : "tasma",
		"position"  : [ -101.961, -44.843, -276.777 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_mysliwce_tasma" : {
		"geometry"  : "geo_projektor.005",
		"groups"    : [  ],
		"material"  : "tasma",
		"position"  : [ -102.938, -50.346, -273.849 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"ekran_statyw_mysliwce_rury" : {
		"geometry"  : "geo_ekran_statyw_mysliwce.000",
		"groups"    : [  ],
		"material"  : "srebro",
		"position"  : [ -102.807, -41.5426, -353.053 ],
		"rotation"  : [ -4.12694e-08, 0, 0 ],
		"quaternion": [ -2.06347e-08, 0, 0, 1 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"projektor_mysliwce_metal" : {
		"geometry"  : "geo_projektor.004",
		"groups"    : [  ],
		"material"  : "srebro",
		"position"  : [ -102.938, -50.346, -273.849 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"pulpit_mysliwce_napis" : {
		"geometry"  : "geo_pulpit_mysliwce_napis",
		"groups"    : [  ],
		"material"  : "srebro",
		"position"  : [ -234.767, -48.9636, -151.45 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"projektor_mysliwce_rolka_dol" : {
		"geometry"  : "geo_rolka_gora.002",
		"groups"    : [  ],
		"material"  : "rolka",
		"position"  : [ -101.961, -54.98, -276.743 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"scena_bombowce_krawedz_cien" : {
		"geometry"  : "geo_scena_mysliwce_krawedz.003",
		"groups"    : [  ],
		"material"  : "scena_krawedz",
		"position"  : [ -413.141, -80.7276, -353.533 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"scena_bombowce_krawedz" : {
		"geometry"  : "geo_scena_mysliwce_krawedz.001",
		"groups"    : [  ],
		"material"  : "scena_krawedz",
		"position"  : [ -413.141, -80.7276, -353.533 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"pulpit_bombowce" : {
		"geometry"  : "geo_pulpit_bomowce",
		"groups"    : [  ],
		"material"  : "pulpit",
		"position"  : [ -234.767, -48.9636, -151.45 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"pulpit_bombowce_napis" : {
		"geometry"  : "geo_Mesh",
		"groups"    : [  ],
		"material"  : "srebro",
		"position"  : [ 213.81, -43.9551, 367.8 ],
		"rotation"  : [ -0, -1.5708, -3.42286e-08 ],
		"quaternion": [ -1.45909e-08, -0.707107, -3.87942e-08, 0.707107 ],
		"scale"     : [ 2.28852, 2.28852, 2.28852 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"podloga" : {
		"geometry"  : "geo_podloga",
		"groups"    : [  ],
		"material"  : "podloga",
		"position"  : [ -260.979, -54.1475, -258.452 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : true,
		"doubleSided"   : true
	},

	"scena_mysliwce_krawedz_cien" : {
		"geometry"  : "geo_scena_mysliwce_krawedz.002",
		"groups"    : [  ],
		"material"  : "scena_krawedz",
		"position"  : [ -413.141, -80.7276, -353.533 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"projektor_bombowce_wlacznik" : {
		"geometry"  : "geo_projektor.003",
		"groups"    : [  ],
		"material"  : "srebro_wlacznik",
		"position"  : [ 336.964, -50.346, 505.132 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_mysliwce_wlacznik" : {
		"geometry"  : "geo_projektor.002",
		"groups"    : [  ],
		"material"  : "srebro_wlacznik",
		"position"  : [ -104.808, -49.3229, -269.367 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"messerschmitt_szyba" : {
		"geometry"  : "geo_Cylinder.003",
		"groups"    : [  ],
		"material"  : "szyba_mess",
		"position"  : [ -414.38, -44.2267, -371.473 ],
		"rotation"  : [ -1.83493, -1.26193e-17, -3.02015e-18 ],
		"quaternion": [ -0.794063, -5.03432e-18, 4.09237e-18, 0.607836 ],
		"scale"     : [ 24.2068, 24.2068, 24.2068 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"zeszyt_mysliwce_plane" : {
		"geometry"  : "geo_Plane",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ -234.368, -51.3637, -144.778 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"zeszyt_otwarty_stol_bombowce" : {
		"geometry"  : "geo_zeszyt_otwarty_pulpit_bombowce.001",
		"groups"    : [  ],
		"material"  : "zeszyt_stol",
		"position"  : [ 275.403, -68.3233, 432.865 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"zeszyt_otwarty_stol_mysliwce" : {
		"geometry"  : "geo_zeszyt_otwarty_pulpit_mysliwce.001",
		"groups"    : [  ],
		"material"  : "zeszyt_stol",
		"position"  : [ -174.609, -68.3232, -212.13 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_bombowce_rolka_gora" : {
		"geometry"  : "geo_rolka_gora.001",
		"groups"    : [  ],
		"material"  : "rolka",
		"position"  : [ 339.892, -44.843, 506.109 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_bombowce_rolka_dol" : {
		"geometry"  : "geo_rolka_dol.001",
		"groups"    : [  ],
		"material"  : "rolka",
		"position"  : [ 339.858, -54.98, 506.109 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_bombowce" : {
		"geometry"  : "geo_projektor.001",
		"groups"    : [  ],
		"material"  : "projektor",
		"position"  : [ 336.964, -50.346, 505.132 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"zarowka1" : {
		"geometry"  : "geo_lampa.008",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ 459.745, 205.864, 228.386 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"zarowka4" : {
		"geometry"  : "geo_lampa.007",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ 459.745, 205.864, -353.535 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"zarowka2" : {
		"geometry"  : "geo_lampa.005",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ -413.136, 205.864, -353.535 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"zarowka3" : {
		"geometry"  : "geo_lampa.004",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ -413.136, 205.864, 228.386 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"lampa4" : {
		"geometry"  : "geo_lampa.003",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ 459.739, 210.875, -353.534 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"lampa3" : {
		"geometry"  : "geo_lampa.002",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ -413.141, 210.875, 228.387 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"lampa2" : {
		"geometry"  : "geo_lampa.000",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ -413.141, 210.875, -353.534 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"podstawa_napis_mysliwce" : {
		"geometry"  : "geo_podstawa_mysliwce.001",
		"groups"    : [  ],
		"material"  : "srebro_napis_mysliwce",
		"position"  : [ -414.66, -81.0175, -83.5968 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"podstawa_napis_bombowce" : {
		"geometry"  : "geo_podstawa_bombowce.001",
		"groups"    : [  ],
		"material"  : "srebro_napis_bombowce",
		"position"  : [ 58.2281, -80.4117, 140.931 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"ekran_statyw_bombowce" : {
		"geometry"  : "geo_ekran_statyw_bombowce.001",
		"groups"    : [  ],
		"material"  : "ekran",
		"position"  : [ 415.897, -41.5426, 505.5 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"blat_pod_kamere_bombowce" : {
		"geometry"  : "geo_blat_pod_kamere_bombowce.001",
		"groups"    : [  ],
		"material"  : "stolik",
		"position"  : [ 336.366, -72.2715, 504.979 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"stol_bombowce" : {
		"geometry"  : "geo_stol_bombowce",
		"groups"    : [  ],
		"material"  : "stol",
		"position"  : [ 280.302, -77.3356, 434.436 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"ksiazka_bombowce" : {
		"geometry"  : "geo_ksiazka_bombowce.001",
		"groups"    : [  ],
		"material"  : "ksiazki",
		"position"  : [ 291.361, -66.4698, 443.833 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"zeszyt_mysliwce_bombowce" : {
		"geometry"  : "geo_zeszyt_mysliwce_bombowce",
		"groups"    : [  ],
		"material"  : "zeszyty",
		"position"  : [ 290.683, -66.517, 443.2 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"zeszyt_otwarty_pulpit_bombowce" : {
		"geometry"  : "geo_zeszyt_otwarty_pulpit_bombowce",
		"groups"    : [  ],
		"material"  : "zeszyt",
		"position"  : [ 208.444, -51.5752, 372.642 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"ekran_bombowce" : {
		"geometry"  : "geo_ekran_bombowce.001",
		"groups"    : [  ],
		"material"  : "projektor_ekran",
		"position"  : [ 415.25, -45.8804, 505.471 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : true
	},

	"scena_mysliwiec_barierka_slupki" : {
		"geometry"  : "geo_scena_mysliwiec_barierka_slupki",
		"groups"    : [  ],
		"material"  : "Gold",
		"position"  : [ -413.709, -57.7501, -352.551 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"scena_bombowiec_barierka_slupki" : {
		"geometry"  : "geo_scena_bombowiec_barierka_slupki",
		"groups"    : [  ],
		"material"  : "Gold",
		"position"  : [ 519.374, -58.3443, -44.8793 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"podstawa_bombowce" : {
		"geometry"  : "geo_podstawa_bombowce",
		"groups"    : [  ],
		"material"  : "szyld_bombowce",
		"position"  : [ 83.0042, -80.5692, 135.934 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"scena_bombowce" : {
		"geometry"  : "geo_scena_bombowce",
		"groups"    : [  ],
		"material"  : "scene",
		"position"  : [ 518.059, -81.9097, -43.9137 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"scena_bombowiec_barierka_lina" : {
		"geometry"  : "geo_scena_bombowiec_barierka_lina",
		"groups"    : [  ],
		"material"  : "lina",
		"position"  : [ 517.948, -51.5538, -43.8109 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : true
	},

	"ekran_mysliwce" : {
		"geometry"  : "geo_ekran_mysliwce.001",
		"groups"    : [  ],
		"material"  : "projektor_ekran",
		"position"  : [ -102.832, -45.8803, -352.405 ],
		"rotation"  : [ -4.12694e-08, 0, 0 ],
		"quaternion": [ -2.06347e-08, 0, 0, 1 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : true
	},

	"lampa1" : {
		"geometry"  : "geo_lampa.001",
		"groups"    : [  ],
		"material"  : "",
		"position"  : [ 459.739, 210.875, 228.387 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"muzeum" : {
		"geometry"  : "geo_muzeum",
		"groups"    : [  ],
		"material"  : "wall",
		"position"  : [ -260.979, -54.1475, -258.452 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : true,
		"doubleSided"   : true
	},

	"scena_mysliwiec_barierka_linka" : {
		"geometry"  : "geo_scena_mysliwiec_barierka_linka",
		"groups"    : [  ],
		"material"  : "lina",
		"position"  : [ -413.141, -51.6923, -353.533 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : true
	},

	"zeszyt_otwarty_pulpit_mysliwce" : {
		"geometry"  : "geo_zeszyt_otwarty_pulpit_mysliwce",
		"groups"    : [  ],
		"material"  : "zeszyt",
		"position"  : [ -234.435, -51.5752, -144.817 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"pulpit_mysliwce" : {
		"geometry"  : "geo_pulpit_mysliwce",
		"groups"    : [  ],
		"material"  : "pulpit",
		"position"  : [ -234.767, -48.9636, -151.45 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"zeszyt_mysliwce_1" : {
		"geometry"  : "geo_zeszyt_mysliwce_1",
		"groups"    : [  ],
		"material"  : "zeszyty",
		"position"  : [ -164.364, -66.517, -227.471 ],
		"rotation"  : [ -1.5708, -3.76625e-08, 0.930977 ],
		"quaternion": [ -0.631872, 0.317392, 0.317392, 0.631872 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"ksiazka_mysliwce_1" : {
		"geometry"  : "geo_ksiazka_mysliwce_1.001",
		"groups"    : [  ],
		"material"  : "ksiazki",
		"position"  : [ -163.735, -66.4697, -228.153 ],
		"rotation"  : [ -1.5708, -4.61109e-08, 0.776465 ],
		"quaternion": [ -0.654484, 0.267677, 0.267677, 0.654484 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"stol_mysliwce" : {
		"geometry"  : "geo_stol_mysliwce",
		"groups"    : [  ],
		"material"  : "stol",
		"position"  : [ -173.067, -77.3355, -217.039 ],
		"rotation"  : [ -1.5708, 1.99003e-08, -0.670481 ],
		"quaternion": [ -0.667743, -0.232636, -0.232636, 0.667743 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"blat_pod_kamere_mysliwce" : {
		"geometry"  : "geo_blat_pod_kamere_mysliwce.001",
		"groups"    : [  ],
		"material"  : "stolik",
		"position"  : [ -102.857, -72.2715, -273.519 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"ekran_statyw_mysliwce" : {
		"geometry"  : "geo_ekran_statyw_mysliwce.001",
		"groups"    : [  ],
		"material"  : "ekran",
		"position"  : [ -102.807, -41.5426, -353.053 ],
		"rotation"  : [ -4.12694e-08, 0, 0 ],
		"quaternion": [ -2.06347e-08, 0, 0, 1 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"podstawa_mysliwce" : {
		"geometry"  : "geo_podstawa_mysliwce",
		"groups"    : [  ],
		"material"  : "szyld_mysliwce",
		"position"  : [ -418.023, -79.055, -104.94 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"scena_mysliwce" : {
		"geometry"  : "geo_scena_mysliwce",
		"groups"    : [  ],
		"material"  : "scene",
		"position"  : [ -413.141, -82.0682, -353.533 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : true,
		"doubleSided"   : true
	},

	"scena_mysliwce_krawedz" : {
		"geometry"  : "geo_scena_mysliwce_krawedz",
		"groups"    : [  ],
		"material"  : "scena_krawedz",
		"position"  : [ -413.141, -80.7276, -353.533 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_mysliwce" : {
		"geometry"  : "geo_projektor",
		"groups"    : [  ],
		"material"  : "projektor",
		"position"  : [ -102.938, -50.346, -273.849 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"projektor_mysliwce_rolka_gora" : {
		"geometry"  : "geo_rolka_gora",
		"groups"    : [  ],
		"material"  : "rolka",
		"position"  : [ -101.961, -44.843, -276.777 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"spitfire" : {
		"geometry"  : "geo_Cylinder.001",
		"groups"    : [  ],
		"material"  : "spitfire",
		"position"  : [ -414.38, -41.3194, -335.882 ],
		"rotation"  : [ -1.77648, -4.53519e-17, -4.68052e-18 ],
		"quaternion": [ -0.775964, -1.61194e-17, 1.61195e-17, 0.630777 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"spitfire_szyba" : {
		"geometry"  : "geo_Cylinder.000",
		"groups"    : [  ],
		"material"  : "szyba_spit",
		"position"  : [ -414.539, -22.7706, -360.069 ],
		"rotation"  : [ -1.77649, -4.53518e-17, -4.68083e-18 ],
		"quaternion": [ -0.775966, -1.61195e-17, 1.61195e-17, 0.630775 ],
		"scale"     : [ 29.096, 29.096, 29.096 ],
		"visible"       : true,
		"castShadow"    : false,
		"receiveShadow" : false,
		"doubleSided"   : false
	},

	"messerschmitt" : {
		"geometry"  : "geo_Cylinder.002",
		"groups"    : [  ],
		"material"  : "messerschmitt",
		"position"  : [ -414.38, -44.2267, -371.473 ],
		"rotation"  : [ -1.83493, -1.26193e-17, -3.02015e-18 ],
		"quaternion": [ -0.794063, -5.03432e-18, 4.09237e-18, 0.607836 ],
		"scale"     : [ 24.2068, 24.2068, 24.2068 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"junkers_kadlub" : {
		"geometry"  : "geo_Cube.008",
		"groups"    : [  ],
		"material"  : "kadlub",
		"position"  : [ 547.583, -10.8103, -55.0442 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : true
	},

	"junkers_ogon" : {
		"geometry"  : "geo_Cube.022",
		"groups"    : [  ],
		"material"  : "ogon",
		"position"  : [ 547.583, -10.8103, -55.0442 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"junkers_silniki" : {
		"geometry"  : "geo_Cube.026",
		"groups"    : [  ],
		"material"  : "silniki",
		"position"  : [ 547.583, -10.8103, -55.0442 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : true
	},

	"junkers_skrzydla" : {
		"geometry"  : "geo_Cube.024",
		"groups"    : [  ],
		"material"  : "skrzydla",
		"position"  : [ 547.583, -10.8103, -55.0442 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : true,
		"doubleSided"   : false
	},

	"junkers_szklo" : {
		"geometry"  : "geo_Cube.027",
		"groups"    : [  ],
		"material"  : "szyba_jun",
		"position"  : [ 547.583, -10.8103, -55.0442 ],
		"rotation"  : [ -1.5708, 0, 0 ],
		"quaternion": [ -0.707107, 0, 0, 0.707107 ],
		"scale"     : [ 1, 1, 1 ],
		"visible"       : true,
		"castShadow"    : true,
		"receiveShadow" : false,
		"doubleSided"   : false
	}
},


"geometries" :
{
	"geo_muzeum.001" : {
		"type" : "ascii",
		"url"  : "muzeum.muzeum.001.js"
	},

	"geo_projektor.011" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.011.js"
	},

	"geo_projektor.010" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.010.js"
	},

	"geo_projektor.008" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.008.js"
	},

	"geo_projektor.006" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.006.js"
	},

	"geo_rolka_dol.002" : {
		"type" : "ascii",
		"url"  : "muzeum.rolka_dol.002.js"
	},

	"geo_rolka_gora.005" : {
		"type" : "ascii",
		"url"  : "muzeum.rolka_gora.005.js"
	},

	"geo_ekran_statyw_bombowce.000" : {
		"type" : "ascii",
		"url"  : "muzeum.ekran_statyw_bombowce.000.js"
	},

	"geo_rolka_gora.004" : {
		"type" : "ascii",
		"url"  : "muzeum.rolka_gora.004.js"
	},

	"geo_rolka_gora.003" : {
		"type" : "ascii",
		"url"  : "muzeum.rolka_gora.003.js"
	},

	"geo_projektor.005" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.005.js"
	},

	"geo_ekran_statyw_mysliwce.000" : {
		"type" : "ascii",
		"url"  : "muzeum.ekran_statyw_mysliwce.000.js"
	},

	"geo_projektor.004" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.004.js"
	},

	"geo_pulpit_mysliwce_napis" : {
		"type" : "ascii",
		"url"  : "muzeum.pulpit_mysliwce_napis.js"
	},

	"geo_rolka_gora.002" : {
		"type" : "ascii",
		"url"  : "muzeum.rolka_gora.002.js"
	},

	"geo_scena_mysliwce_krawedz.003" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_mysliwce_krawedz.003.js"
	},

	"geo_scena_mysliwce_krawedz.001" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_mysliwce_krawedz.001.js"
	},

	"geo_pulpit_bomowce" : {
		"type" : "ascii",
		"url"  : "muzeum.pulpit_bomowce.js"
	},

	"geo_Mesh" : {
		"type" : "ascii",
		"url"  : "muzeum.Mesh.js"
	},

	"geo_podloga" : {
		"type" : "ascii",
		"url"  : "muzeum.podloga.js"
	},

	"geo_scena_mysliwce_krawedz.002" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_mysliwce_krawedz.002.js"
	},

	"geo_projektor.003" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.003.js"
	},

	"geo_projektor.002" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.002.js"
	},

	"geo_Cylinder.003" : {
		"type" : "ascii",
		"url"  : "muzeum.Cylinder.003.js"
	},

	"geo_Plane" : {
		"type" : "ascii",
		"url"  : "muzeum.Plane.js"
	},

	"geo_zeszyt_otwarty_pulpit_bombowce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.zeszyt_otwarty_pulpit_bombowce.001.js"
	},

	"geo_zeszyt_otwarty_pulpit_mysliwce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.zeszyt_otwarty_pulpit_mysliwce.001.js"
	},

	"geo_rolka_gora.001" : {
		"type" : "ascii",
		"url"  : "muzeum.rolka_gora.001.js"
	},

	"geo_rolka_dol.001" : {
		"type" : "ascii",
		"url"  : "muzeum.rolka_dol.001.js"
	},

	"geo_projektor.001" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.001.js"
	},

	"geo_lampa.008" : {
		"type" : "ascii",
		"url"  : "muzeum.lampa.008.js"
	},

	"geo_lampa.007" : {
		"type" : "ascii",
		"url"  : "muzeum.lampa.007.js"
	},

	"geo_lampa.005" : {
		"type" : "ascii",
		"url"  : "muzeum.lampa.005.js"
	},

	"geo_lampa.004" : {
		"type" : "ascii",
		"url"  : "muzeum.lampa.004.js"
	},

	"geo_lampa.003" : {
		"type" : "ascii",
		"url"  : "muzeum.lampa.003.js"
	},

	"geo_lampa.002" : {
		"type" : "ascii",
		"url"  : "muzeum.lampa.002.js"
	},

	"geo_lampa.000" : {
		"type" : "ascii",
		"url"  : "muzeum.lampa.000.js"
	},

	"geo_podstawa_mysliwce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.podstawa_mysliwce.001.js"
	},

	"geo_podstawa_bombowce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.podstawa_bombowce.001.js"
	},

	"geo_ekran_statyw_bombowce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.ekran_statyw_bombowce.001.js"
	},

	"geo_blat_pod_kamere_bombowce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.blat_pod_kamere_bombowce.001.js"
	},

	"geo_stol_bombowce" : {
		"type" : "ascii",
		"url"  : "muzeum.stol_bombowce.js"
	},

	"geo_ksiazka_bombowce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.ksiazka_bombowce.001.js"
	},

	"geo_zeszyt_mysliwce_bombowce" : {
		"type" : "ascii",
		"url"  : "muzeum.zeszyt_mysliwce_bombowce.js"
	},

	"geo_zeszyt_otwarty_pulpit_bombowce" : {
		"type" : "ascii",
		"url"  : "muzeum.zeszyt_otwarty_pulpit_bombowce.js"
	},

	"geo_ekran_bombowce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.ekran_bombowce.001.js"
	},

	"geo_scena_mysliwiec_barierka_slupki" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_mysliwiec_barierka_slupki.js"
	},

	"geo_scena_bombowiec_barierka_slupki" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_bombowiec_barierka_slupki.js"
	},

	"geo_podstawa_bombowce" : {
		"type" : "ascii",
		"url"  : "muzeum.podstawa_bombowce.js"
	},

	"geo_scena_bombowce" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_bombowce.js"
	},

	"geo_scena_bombowiec_barierka_lina" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_bombowiec_barierka_lina.js"
	},

	"geo_ekran_mysliwce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.ekran_mysliwce.001.js"
	},

	"geo_lampa.001" : {
		"type" : "ascii",
		"url"  : "muzeum.lampa.001.js"
	},

	"geo_muzeum" : {
		"type" : "ascii",
		"url"  : "muzeum.muzeum.js"
	},

	"geo_scena_mysliwiec_barierka_linka" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_mysliwiec_barierka_linka.js"
	},

	"geo_zeszyt_otwarty_pulpit_mysliwce" : {
		"type" : "ascii",
		"url"  : "muzeum.zeszyt_otwarty_pulpit_mysliwce.js"
	},

	"geo_pulpit_mysliwce" : {
		"type" : "ascii",
		"url"  : "muzeum.pulpit_mysliwce.js"
	},

	"geo_zeszyt_mysliwce_1" : {
		"type" : "ascii",
		"url"  : "muzeum.zeszyt_mysliwce_1.js"
	},

	"geo_ksiazka_mysliwce_1.001" : {
		"type" : "ascii",
		"url"  : "muzeum.ksiazka_mysliwce_1.001.js"
	},

	"geo_stol_mysliwce" : {
		"type" : "ascii",
		"url"  : "muzeum.stol_mysliwce.js"
	},

	"geo_blat_pod_kamere_mysliwce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.blat_pod_kamere_mysliwce.001.js"
	},

	"geo_ekran_statyw_mysliwce.001" : {
		"type" : "ascii",
		"url"  : "muzeum.ekran_statyw_mysliwce.001.js"
	},

	"geo_podstawa_mysliwce" : {
		"type" : "ascii",
		"url"  : "muzeum.podstawa_mysliwce.js"
	},

	"geo_scena_mysliwce" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_mysliwce.js"
	},

	"geo_scena_mysliwce_krawedz" : {
		"type" : "ascii",
		"url"  : "muzeum.scena_mysliwce_krawedz.js"
	},

	"geo_projektor" : {
		"type" : "ascii",
		"url"  : "muzeum.projektor.js"
	},

	"geo_rolka_gora" : {
		"type" : "ascii",
		"url"  : "muzeum.rolka_gora.js"
	},

	"geo_Cylinder.001" : {
		"type" : "ascii",
		"url"  : "muzeum.Cylinder.001.js"
	},

	"geo_Cylinder.000" : {
		"type" : "ascii",
		"url"  : "muzeum.Cylinder.000.js"
	},

	"geo_Cylinder.002" : {
		"type" : "ascii",
		"url"  : "muzeum.Cylinder.002.js"
	},

	"geo_Cube.008" : {
		"type" : "ascii",
		"url"  : "muzeum.Cube.008.js"
	},

	"geo_Cube.022" : {
		"type" : "ascii",
		"url"  : "muzeum.Cube.022.js"
	},

	"geo_Cube.026" : {
		"type" : "ascii",
		"url"  : "muzeum.Cube.026.js"
	},

	"geo_Cube.024" : {
		"type" : "ascii",
		"url"  : "muzeum.Cube.024.js"
	},

	"geo_Cube.027" : {
		"type" : "ascii",
		"url"  : "muzeum.Cube.027.js"
	}
},


"textures" :
{
	"texture_Supermarine Spitfire Mk_II_023.png" : {
		"url": "texture_Spitfire_Mk_II.png",
        "wrap": ["repeat", "repeat"]
	},

	"podloga_dif.png" : {
		"url": "podloga_dif.png",
        "wrap": ["repeat", "repeat"]
	},

	"pulpit_dif.png" : {
		"url": "pulpit_dif.png",
        "wrap": ["repeat", "repeat"]
	},

	"zeszyt_otwarty.png" : {
		"url": "zeszyt_otwarty.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_kadlub_Junkers_Ju_88_A4.png" : {
		"url": "tex_kadlub_Junkers_Ju_88_A4.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_ogon_Junkers_Ju_88_A4.png" : {
		"url": "tex_ogon_Junkers_Ju_88_A4.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_silniki_Junkers_Ju_88_A4.png" : {
		"url": "tex_silniki_Junkers_Ju_88_A4.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_skrzydla_Junkers_Ju_88_A4.png" : {
		"url": "tex_skrzydla_Junkers_Ju_88_A4.png",
        "wrap": ["repeat", "repeat"]
	},

	"wall.jpg" : {
		"url": "wall.jpg",
        "wrap": ["repeat", "repeat"]
	},

	"tex_ekran.png" : {
		"url": "tex_ekran.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_ksiazki.png" : {
		"url": "tex_ksiazki.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_zeszyty.png" : {
		"url": "tex_zeszyty.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_stol.png" : {
		"url": "tex_stol.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_lina.png" : {
		"url": "tex_lina.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_projektor.png" : {
		"url": "tex_projektor.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_stolik.png" : {
		"url": "tex_stolik.png",
        "wrap": ["repeat", "repeat"]
	},

	"webtreats-seamless-cloud-3.jpg" : {
		"url": "webtreats-seamless-cloud-3.jpg",
        "wrap": ["repeat", "repeat"]
	},

	"tex_scene.png" : {
		"url": "scena.jpg",
        "wrap": ["repeat", "repeat"]
	},

	"tex_scena_krawedz.jpg" : {
		"url": "tex_scena_krawedz.jpg",
        "wrap": ["repeat", "repeat"]
	},

	"tex_podstawa.png" : {
		"url": "tex_podstawa.png",
        "wrap": ["repeat", "repeat"]
	},

	"tex_messerschmitt.png" : {
		"url": "tex_messerschmitt.png",
        "wrap": ["repeat", "repeat"]
	}
},


"materials" :
{
	"czarny" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 0, "opacity": 1, "blending": "NormalBlending", "depthWrite": false }
	},

	"ekran" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "map": "tex_ekran.png", "blending": "NormalBlending" }
	},

	"Gold" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 13415681, "opacity": 1, "ambient": 13415681, "specular": 16775366, "shininess": 4e+01, "blending": "NormalBlending" }
	},

	"kadlub" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 13421772, "opacity": 1, "ambient": 13421772, "specular": 197379, "shininess": 4e+01, "map": "tex_kadlub_Junkers_Ju_88_A4.png", "blending": "NormalBlending" }
	},

	"ksiazki" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "map": "tex_ksiazki.png", "blending": "NormalBlending" }
	},

	"lina" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 8355711, "shininess": 2e+02, "map": "tex_lina.png", "blending": "NormalBlending" }
	},

	"messerschmitt" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 4605510, "shininess": 5e+01, "map": "tex_messerschmitt.png", "blending": "NormalBlending" }
	},

	"ogon" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 13421772, "opacity": 1, "ambient": 13421772, "specular": 197379, "shininess": 4e+01, "map": "tex_ogon_Junkers_Ju_88_A4.png", "blending": "NormalBlending" }
	},

	"podloga" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 10066329, "shininess": 5e+02, "map": "podloga_dif.png", "blending": "NormalBlending" }
	},

	"projektor" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 10705985, "opacity": 1, "map": "tex_projektor.png", "blending": "NormalBlending" }
	},

	"projektor_ekran" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 13421772, "opacity": 1, "blending": "NormalBlending" }
	},

	"pulpit" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 7497013, "opacity": 1, "ambient": 7497013, "specular": 7368816, "shininess": 5e+01, "map": "pulpit_dif.png", "blending": "NormalBlending" }
	},

	"rolka" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 5865635, "opacity": 1, "blending": "NormalBlending" }
	},

	"scena_krawedz" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 8026746, "opacity": 1, "ambient": 8026746, "specular": 7368816, "shininess": 5e+01, "map": "tex_scena_krawedz.jpg", "blending": "NormalBlending" }
	},

	"scene" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 8355711, "shininess": 5e+01, "map": "tex_scene.png", "blending": "NormalBlending" }
	},

	"silniki" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 13421772, "opacity": 1, "ambient": 13421772, "specular": 197379, "shininess": 4e+01, "map": "tex_silniki_Junkers_Ju_88_A4.png", "blending": "NormalBlending" }
	},

	"skrzydla" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 13421772, "opacity": 1, "ambient": 13421772, "specular": 197379, "shininess": 4e+01, "map": "tex_skrzydla_Junkers_Ju_88_A4.png", "blending": "NormalBlending" }
	},

	"spitfire" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 4605510, "shininess": 5e+01, "map": "texture_Supermarine Spitfire Mk_II_023.png", "blending": "NormalBlending" }
	},

	"srebro" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 11316415, "opacity": 1, "ambient": 11316415, "specular": 16777215, "shininess": 2e+02, "blending": "NormalBlending" }
	},

	"srebro_napis_bombowce" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10263726, "opacity": 1, "ambient": 10263726, "specular": 3815994, "shininess": 9e+01, "blending": "NormalBlending" }
	},

	"srebro_napis_mysliwce" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10263726, "opacity": 1, "ambient": 10263726, "specular": 3815994, "shininess": 9e+01, "blending": "NormalBlending" }
	},

	"srebro_wlacznik" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10263726, "opacity": 1, "ambient": 10263726, "specular": 3815994, "shininess": 9e+01, "blending": "NormalBlending" }
	},

	"stol" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 8026746, "opacity": 1, "map": "tex_stol.png", "blending": "NormalBlending" }
	},

	"stolik" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 8026746, "opacity": 1, "map": "tex_stolik.png", "blending": "NormalBlending" }
	},

	"sufit" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 1644825, "shininess": 5e+01, "map": "wall.jpg", "blending": "NormalBlending" }
	},

	"szyba_jun" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 11062473, "opacity": 1, "ambient": 11062473, "specular": 8355711, "shininess": 1e+02, "blending": "NormalBlending" }
	},

	"szyba_mess" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 8355711, "shininess": 1e+02, "blending": "NormalBlending" }
	},

	"szyba_spit" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 8355711, "shininess": 1e+02, "blending": "NormalBlending" }
	},

	"szyld_bombowce" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 8355711, "shininess": 5e+01, "map": "tex_podstawa.png", "blending": "NormalBlending" }
	},

	"szyld_mysliwce" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 8355711, "shininess": 5e+01, "map": "tex_podstawa.png", "blending": "NormalBlending" }
	},

	"tasma" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 131072, "opacity": 1, "blending": "NormalBlending" }
	},

	"wall" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "ambient": 10724259, "specular": 1644825, "shininess": 5e+01, "map": "wall.jpg", "blending": "NormalBlending" }
	},

	"zeszyt" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 13421772, "opacity": 1, "ambient": 13421772, "specular": 3684408, "shininess": 4, "map": "zeszyt_otwarty.png", "blending": "NormalBlending" }
	},

	"zeszyt_stol" : {
		"type": "MeshPhongMaterial",
		"parameters": { "color": 13421772, "opacity": 1, "ambient": 13421772, "specular": 3684408, "shininess": 4, "map": "zeszyt_otwarty.png", "blending": "NormalBlending" }
	},

	"zeszyty" : {
		"type": "MeshLambertMaterial",
		"parameters": { "color": 10724259, "opacity": 1, "map": "tex_zeszyty.png", "blending": "NormalBlending" }
	}
},


"transform" :
{
	"position"  : [ 0, 0, 0 ],
	"rotation"  : [ 0, 0, 0 ],
	"scale"     : [ 1, 1, 1 ]
},

"defaults" :
{
	"bgcolor" : [ 0, 0, 0 ],
	"bgalpha" : 1.000000,
	"camera"  : ""
}

}
