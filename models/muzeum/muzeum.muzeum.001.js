{

	"metadata" :
	{
		"formatVersion" : 3.1,
		"generatedBy"   : "Blender 2.66 Exporter",
		"vertices"      : 8,
		"faces"         : 6,
		"normals"       : 1,
		"colors"        : 0,
		"uvs"           : [8],
		"materials"     : 1,
		"morphTargets"  : 0,
		"bones"         : 0
	},

	"scale" : 1.000000,

	"materials" : [	{
		"DbgColor" : 15658734,
		"DbgIndex" : 0,
		"DbgName" : "sufit",
		"blending" : "NormalBlending",
		"colorAmbient" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorDiffuse" : [0.6400000190734865, 0.6400000190734865, 0.6400000190734865],
		"colorSpecular" : [0.10000000149011612, 0.10000000149011612, 0.10000000149011612],
		"depthTest" : true,
		"depthWrite" : true,
		"mapDiffuse" : "wall.jpg",
		"mapDiffuseWrap" : ["repeat", "repeat"],
		"shading" : "Phong",
		"specularCoef" : 50,
		"transparency" : 1.0,
		"transparent" : false,
		"vertexColors" : false
	}],

	"vertices" : [-15.907,-11.3042,11.0397,14.5521,14.1237,11.0397,45.0113,-11.3042,11.0397,-15.907,-36.7322,11.0397,-15.907,14.1237,11.0397,45.0113,14.1237,11.0397,45.0113,-36.7322,11.0397,14.5521,-36.7322,11.0397],

	"morphTargets" : [],

	"normals" : [0,0,-1],

	"colors" : [],

	"uvs" : [[0.5,9.1e-05,0.999908,9.1e-05,0.999909,0.417425,9.1e-05,9.2e-05,9.1e-05,0.417425,0.999909,0.834758,9.1e-05,0.834758,0.5,0.834759]],

	"faces" : [42,1,5,2,0,0,1,2,0,0,0,42,4,1,2,0,3,0,2,0,0,0,42,2,0,4,0,2,4,3,0,0,0,42,6,0,2,0,5,4,2,0,0,0,42,3,0,6,0,6,4,5,0,0,0,42,6,7,3,0,5,7,6,0,0,0],

	"bones" : [],

	"skinIndices" : [],

	"skinWeights" : [],

	"animation" : {}


}
