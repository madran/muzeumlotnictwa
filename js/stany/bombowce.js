function states_bombowce(state) {
    states.push(new TDU.WorldState('bombowce', function(s, fromButton) {
        world.controls = null;
        var pulpit_bombowce = s.world.getObjectByName('pulpit_bombowce');
        pulpit_bombowce.getEvent('pulpit_bombowce_najazd').isActive = true;
        pulpit_bombowce.getEvent('pulpit_bombowce_highlight').isActive = true;

        var zeszyt_otwarty_pulpit_bombowce = s.world.getObjectByName('zeszyt_otwarty_pulpit_bombowce');
        zeszyt_otwarty_pulpit_bombowce.getEvent('pulpit_bombowce_najazd').isActive = true;
        zeszyt_otwarty_pulpit_bombowce.getEvent('zeszyt_otwarty_pulpit_bombowce_highlight').isActive = true;

        var projektor_bombowce = s.world.getObjectByName('projektor_bombowce');
        projektor_bombowce.getEvent('najazd_na_ekran_bombowce').isActive = true;
        projektor_bombowce.getEvent('projektor_bombowce_highlight').isActive = true;

        var projektor_bombowce_rolka_gora = s.world.getObjectByName('projektor_bombowce_rolka_gora');
        projektor_bombowce_rolka_gora.getEvent('najazd_na_ekran_bombowce').isActive = true;
        projektor_bombowce_rolka_gora.getEvent('projektor_bombowce_rolka_gora_highlight').isActive = true;

        var projektor_bombowce_rolka_dol = s.world.getObjectByName('projektor_bombowce_rolka_dol');
        projektor_bombowce_rolka_dol.getEvent('najazd_na_ekran_bombowce').isActive = true;
        projektor_bombowce_rolka_dol.getEvent('projektor_bombowce_rolka_dol_highlight').isActive = true;

        var podstawa_bombowce = s.world.getObjectByName('podstawa_bombowce');
        podstawa_bombowce.getEvent('bombowce_plan_ogolny').isActive = false;
        podstawa_bombowce.getEvent('podstawa_bombowce_highlight').isActive = false;

        var podstawa_napis_bombowce = s.world.getObjectByName('podstawa_napis_bombowce');
        podstawa_napis_bombowce.getEvent('bombowce_plan_ogolny').isActive = false;
        podstawa_napis_bombowce.getEvent('podstawa_napis_bombowce_highlight').isActive = false;

        var spitfire = world.getObjectByName('spitfire');
        spitfire.getEvent('spitfire_zoom').isActive = true;
        spitfire.getEvent('spitfire_highlight').isActive = true;

        var spitfire_szyba = s.world.getObjectByName('spitfire_szyba');
        spitfire_szyba.getEvent('spitfire_zoom').isActive = true;
        spitfire_szyba.getEvent('spitfire_szyba_highlight').isActive = true;

        var messerschmitt = world.getObjectByName('messerschmitt');
        messerschmitt.getEvent('messerschmitt_zoom').isActive = true;
        messerschmitt.getEvent('messerschmitt_highlight').isActive = true;

        var messerschmitt_szyba = s.world.getObjectByName('messerschmitt_szyba');
        messerschmitt_szyba.getEvent('messerschmitt_zoom').isActive = true;
        messerschmitt_szyba.getEvent('messerschmitt_szyba_highlight').isActive = true;

        var junkers_kadlub = world.getObjectByName('junkers_kadlub');
        junkers_kadlub.getEvent('junkers_kadlub_zoom').isActive = true
        junkers_kadlub.getEvent('junkers_kadlub_highlight').isActive = true
        var junkers_skrzydla = world.getObjectByName('junkers_skrzydla');
        junkers_skrzydla.getEvent('junkers_skrzydla_zoom').isActive = true
        junkers_skrzydla.getEvent('junkers_skrzydla_highlight').isActive = true
        var junkers_ogon = world.getObjectByName('junkers_ogon');
        junkers_ogon.getEvent('junkers_ogon_zoom').isActive = true
        junkers_ogon.getEvent('junkers_ogon_highlight').isActive = true
        var junkers_silniki = world.getObjectByName('junkers_silniki');
        junkers_silniki.getEvent('junkers_silniki_zoom').isActive = true
        junkers_silniki.getEvent('junkers_silniki_highlight').isActive = true
        var junkers_szklo = world.getObjectByName('junkers_szklo');
        junkers_szklo.getEvent('junkers_szklo_zoom').isActive = true
        junkers_szklo.getEvent('junkers_szklo_highlight').isActive = true

        outliner.setOutlineOn('spitfire');
        outliner.setOutlineOn('spitfire_szyba');
        outliner.setOutlineOn('messerschmitt');
        outliner.setOutlineOn('messerschmitt_szyba');
        outliner.setOutlineOn('junkers_kadlub');
        outliner.setOutlineOn('junkers_skrzydla');
        outliner.setOutlineOn('junkers_ogon');
        outliner.setOutlineOn('junkers_silniki');
        outliner.setOutlineOn('junkers_szklo');


        outliner.setOutlineOn('pulpit_bombowce');
        outliner.setOutlineOn('projektor_bombowce');
        outliner.setOutlineOn('projektor_bombowce_rolka_gora');
        outliner.setOutlineOn('projektor_bombowce_rolka_dol');
        outliner.removeOutlineFrom('podstawa_bombowce');
        outliner2.removeOutlineFrom('projektor_mysliwce_wlacznik');

        if (fromButton) {
            var tween_pos = new TWEEN.Tween(world.camera.position)
                    .to({x: 89, y: 41.6, z: 612}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
            var tween_rot = new TWEEN.Tween(world.camera.rotation)
                    .to({x: -0.5, y: -0.7, z: -0.3}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
        } else {
            world.camera.position.set(89, 41.6, 612);
            world.camera.rotation.set(-0.5, -0.7, -0.3);
        }

        var messerschmitt = world.getObjectByName('messerschmitt');
        var messerschmitt_szyba = world.getObjectByName('messerschmitt_szyba');
        var spitfire = world.getObjectByName('spitfire');
        var spitfire_szyba = world.getObjectByName('spitfire_szyba');

        if (window.location.hash.split('/')[1] === 'gb') {
            spitfire.render();
            spitfire_szyba.render();

            messerschmitt.doNotRender();
            messerschmitt_szyba.doNotRender();
        }
        if (window.location.hash.split('/')[1] === 'ger') {
            spitfire.doNotRender();
            spitfire_szyba.doNotRender();

            messerschmitt.render();
            messerschmitt_szyba.render();
        }

        var menu_mysliwce = document.getElementById('menu_mysliwce');
        var menu_bombowce = document.getElementById('menu_bombowce');
        var menu_mysliwce_pulpit = document.getElementById('menu_mysliwce_pulpit');
        var menu_bombowce_pulpit = document.getElementById('menu_bombowce_pulpit');
        var menu_mysliwce_projektor = document.getElementById('menu_mysliwce_projektor');
        var menu_bombowce_projektor = document.getElementById('menu_bombowce_projektor');
        var home = document.getElementById('menu_home');

        menu_mysliwce.style.backgroundColor = 'white';
        menu_bombowce.style.backgroundColor = 'black';
        menu_mysliwce_pulpit.style.backgroundColor = 'white';
        menu_bombowce_pulpit.style.backgroundColor = 'white';
        menu_mysliwce_projektor.style.backgroundColor = 'white';
        menu_bombowce_projektor.style.backgroundColor = 'white';
        home.style.backgroundColor = 'white';
    }));
}