function states_mysliwce_projektor(states) {
    states.push(new TDU.WorldState('mysliwce_projektor', function(s, fromButton) {
        world.controls = null;
        var projektor_mysliwce = s.world.getObjectByName('projektor_mysliwce');
        projektor_mysliwce.getEvent('najazd_na_ekran_mysliwce').isActive = false;
        projektor_mysliwce.getEvent('projektor_mysliwce_highlight').isActive = false;

        var projektor_mysliwce_rolka_gora = s.world.getObjectByName('projektor_mysliwce_rolka_gora');
        projektor_mysliwce_rolka_gora.getEvent('najazd_na_ekran_mysliwce').isActive = false;
        projektor_mysliwce_rolka_gora.getEvent('projektor_mysliwce_rolka_gora_highlight').isActive = false;
        projektor_mysliwce_rolka_gora.object.rotation.x = 2 * Math.PI;

        var projektor_mysliwce_rolka_dol = s.world.getObjectByName('projektor_mysliwce_rolka_dol');
        projektor_mysliwce_rolka_dol.getEvent('najazd_na_ekran_mysliwce').isActive = false;
        projektor_mysliwce_rolka_dol.getEvent('projektor_mysliwce_rolka_dol_highlight').isActive = false;
        projektor_mysliwce_rolka_dol.object.rotation.x = 2 * Math.PI;

        var projektor_mysliwce_wlacznik = world.getObjectByName('projektor_mysliwce_wlacznik');
        projektor_mysliwce_wlacznik.getEvent('mysliwce_wlacz_film').isActive = true;
        projektor_mysliwce_wlacznik.getEvent('projektor_mysliwce_wlacznik_highlight').isActive = true;

        var spitfire = world.getObjectByName('spitfire');
        spitfire.getEvent('spitfire_zoom').isActive = true;
        spitfire.getEvent('spitfire_highlight').isActive = true;

        var spitfire_szyba = s.world.getObjectByName('spitfire_szyba');
        spitfire_szyba.getEvent('spitfire_zoom').isActive = true;
        spitfire_szyba.getEvent('spitfire_szyba_highlight').isActive = true;

        var messerschmitt = world.getObjectByName('messerschmitt');
        messerschmitt.getEvent('messerschmitt_zoom').isActive = true;
        messerschmitt.getEvent('messerschmitt_highlight').isActive = true;

        var messerschmitt_szyba = s.world.getObjectByName('messerschmitt_szyba');
        messerschmitt_szyba.getEvent('messerschmitt_zoom').isActive = true;
        messerschmitt_szyba.getEvent('messerschmitt_szyba_highlight').isActive = true;

        outliner.setOutlineOn('spitfire');
        outliner.setOutlineOn('spitfire_szyba');
        outliner.setOutlineOn('messerschmitt');
        outliner.setOutlineOn('messerschmitt_szyba');
        outliner.setOutlineOn('projektor_mysliwce_rolka_gora_tasma');
        outliner.setOutlineOn('projektor_mysliwce_rolka_dol_tasma');

        outliner.removeOutlineFrom('projektor_mysliwce');
        outliner.removeOutlineFrom('projektor_mysliwce_rolka_gora');
        outliner.removeOutlineFrom('projektor_mysliwce_rolka_dol');
        outliner2.setOutlineOn('projektor_mysliwce_wlacznik');

        if (fromButton) {
            var tween_pos = new TWEEN.Tween(world.camera.position)
                    .to({x: -83.6, y: -52.3, z: -247.3}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
            var tween_rot = new TWEEN.Tween(world.camera.rotation)
                    .to({x: 0, y: 0.3, z: 0}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
        } else {
            world.camera.position.set(-83.6, -52.3, -247.3);
            world.camera.rotation.set(0, 0.3, 0);
        }

        var messerschmitt = world.getObjectByName('messerschmitt');
        var messerschmitt_szyba = world.getObjectByName('messerschmitt_szyba');
        var spitfire = world.getObjectByName('spitfire');
        var spitfire_szyba = world.getObjectByName('spitfire_szyba');

        if (window.location.hash.split('/')[1] === 'gb') {
            spitfire.render();
            spitfire_szyba.render();

            messerschmitt.doNotRender();
            messerschmitt_szyba.doNotRender();
        }
        if (window.location.hash.split('/')[1] === 'ger') {
            spitfire.doNotRender();
            spitfire_szyba.doNotRender();

            messerschmitt.render();
            messerschmitt_szyba.render();
        }

        var menu_mysliwce = document.getElementById('menu_mysliwce');
        var menu_bombowce = document.getElementById('menu_bombowce');
        var menu_mysliwce_pulpit = document.getElementById('menu_mysliwce_pulpit');
        var menu_bombowce_pulpit = document.getElementById('menu_bombowce_pulpit');
        var menu_mysliwce_projektor = document.getElementById('menu_mysliwce_projektor');
        var menu_bombowce_projektor = document.getElementById('menu_bombowce_projektor');
        var home = document.getElementById('menu_home');

        menu_mysliwce.style.backgroundColor = 'white';
        menu_bombowce.style.backgroundColor = 'white';
        menu_mysliwce_pulpit.style.backgroundColor = 'white';
        menu_bombowce_pulpit.style.backgroundColor = 'white';
        menu_mysliwce_projektor.style.backgroundColor = 'black';
        menu_bombowce_projektor.style.backgroundColor = 'white';
        home.style.backgroundColor = 'white';
    }));
}