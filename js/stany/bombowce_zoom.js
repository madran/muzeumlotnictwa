function states_bombowce_zoom(states) {
    states.push(new TDU.WorldState('bombowce_zoom', function(s, fromButton) {
        var pulpit_mysliwce = s.world.getObjectByName('pulpit_mysliwce');
        pulpit_mysliwce.getEvent('pulpit_mysliwce_najazd').isActive = false;
        pulpit_mysliwce.getEvent('pulpit_mysliwce_highlight').isActive = false;

        var zeszyt_otwarty_pulpit_mysliwce = s.world.getObjectByName('zeszyt_otwarty_pulpit_mysliwce');
        zeszyt_otwarty_pulpit_mysliwce.getEvent('pulpit_mysliwce_najazd').isActive = false;
        zeszyt_otwarty_pulpit_mysliwce.getEvent('zeszyt_otwarty_pulpit_mysliwce_highlight').isActive = false;

        var projektor_mysliwce = s.world.getObjectByName('projektor_mysliwce');
        projektor_mysliwce.getEvent('najazd_na_ekran_mysliwce').isActive = false;
        projektor_mysliwce.getEvent('projektor_mysliwce_highlight').isActive = false;

        var projektor_mysliwce = s.world.getObjectByName('projektor_mysliwce');
        projektor_mysliwce.getEvent('najazd_na_ekran_mysliwce').isActive = false;
        projektor_mysliwce.getEvent('projektor_mysliwce_highlight').isActive = false;

        var projektor_mysliwce_rolka_gora = s.world.getObjectByName('projektor_mysliwce_rolka_gora');
        projektor_mysliwce_rolka_gora.getEvent('najazd_na_ekran_mysliwce').isActive = false;
        projektor_mysliwce_rolka_gora.getEvent('projektor_mysliwce_rolka_gora_highlight').isActive = false;

        var projektor_mysliwce_rolka_dol = s.world.getObjectByName('projektor_mysliwce_rolka_dol');
        projektor_mysliwce_rolka_dol.getEvent('najazd_na_ekran_mysliwce').isActive = false;
        projektor_mysliwce_rolka_dol.getEvent('projektor_mysliwce_rolka_dol_highlight').isActive = false;

        var podstawa_mysliwce = s.world.getObjectByName('podstawa_mysliwce');
        podstawa_mysliwce.getEvent('mysliwce_plan_ogolny').isActive = false;
        podstawa_mysliwce.getEvent('podstawa_mysliwce_highlight').isActive = false;

        var podstawa_napis_mysliwce = s.world.getObjectByName('podstawa_napis_mysliwce');
        podstawa_napis_mysliwce.getEvent('mysliwce_plan_ogolny').isActive = false;
        podstawa_napis_mysliwce.getEvent('podstawa_napis_mysliwce_highlight').isActive = false;

        var podstawa_bombowce = s.world.getObjectByName('podstawa_bombowce');
        podstawa_bombowce.getEvent('bombowce_plan_ogolny').isActive = false;
        podstawa_bombowce.getEvent('podstawa_bombowce_highlight').isActive = false;

        var podstawa_napis_bombowce = s.world.getObjectByName('podstawa_napis_bombowce');
        podstawa_napis_bombowce.getEvent('bombowce_plan_ogolny').isActive = false;
        podstawa_napis_bombowce.getEvent('podstawa_napis_bombowce_highlight').isActive = false;

        var messerschmitt = world.getObjectByName('messerschmitt');
        messerschmitt.getEvent('messerschmitt_zoom').isActive = false;
        messerschmitt.getEvent('messerschmitt_highlight').isActive = false;

        var messerschmitt_szyba = world.getObjectByName('messerschmitt_szyba');
        messerschmitt_szyba.getEvent('messerschmitt_zoom').isActive = false;
        messerschmitt_szyba.getEvent('messerschmitt_szyba_highlight').isActive = false;

        var spitfire = world.getObjectByName('spitfire');
        spitfire.getEvent('spitfire_zoom').isActive = false;
        spitfire.getEvent('spitfire_highlight').isActive = false;

        var spitfire_szyba = world.getObjectByName('spitfire_szyba');
        spitfire_szyba.getEvent('spitfire_zoom').isActive = false;
        spitfire_szyba.getEvent('spitfire_szyba_highlight').isActive = false;

        var junkers_kadlub = world.getObjectByName('junkers_kadlub');
        junkers_kadlub.getEvent('junkers_kadlub_zoom').isActive = false
        junkers_kadlub.getEvent('junkers_kadlub_highlight').isActive = false
        var junkers_skrzydla = world.getObjectByName('junkers_skrzydla');
        junkers_skrzydla.getEvent('junkers_skrzydla_zoom').isActive = false
        junkers_skrzydla.getEvent('junkers_skrzydla_highlight').isActive = false
        var junkers_ogon = world.getObjectByName('junkers_ogon');
        junkers_ogon.getEvent('junkers_ogon_zoom').isActive = false
        junkers_ogon.getEvent('junkers_ogon_highlight').isActive = false
        var junkers_silniki = world.getObjectByName('junkers_silniki');
        junkers_silniki.getEvent('junkers_silniki_zoom').isActive = false
        junkers_silniki.getEvent('junkers_silniki_highlight').isActive = false
        var junkers_szklo = world.getObjectByName('junkers_szklo');
        junkers_szklo.getEvent('junkers_szklo_zoom').isActive = false
        junkers_szklo.getEvent('junkers_szklo_highlight').isActive = false

        if (fromButton) {
            var tween_pos = new TWEEN.Tween(world.camera.position)
                    .to({x: 300.3, y: 44.4, z: 179.8}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
            var tween_rot = new TWEEN.Tween(world.camera.rotation)
                    .to({x: -0.29, y: -0.91, z: -0.23}, 2000)
                    .easing(TWEEN.Easing.Linear.None).onComplete(bombowce_zoom_camera).start();
        } else {
            world.camera.position.set(300.3, 44.4, 179.8);
            world.camera.rotation.set(-0.29, -0.91, -0.23);
            bombowce_zoom_camera();
        }

        outliner.removeOutlineFrom('pulpit_mysliwce');
        outliner.removeOutlineFrom('projektor_mysliwce');
        outliner.removeOutlineFrom('projektor_mysliwce_rolka_gora');
        outliner.removeOutlineFrom('projektor_mysliwce_rolka_dol');
        outliner.removeOutlineFrom('podstawa_mysliwce');
        outliner.removeOutlineFrom('projektor_mysliwce_wlacznik');
        outliner.removeOutlineFrom('spitfire');
        outliner.removeOutlineFrom('spitfire_szyba');
        outliner.removeOutlineFrom('messerschmitt');
        outliner.removeOutlineFrom('messerschmitt_szyba');
        outliner.removeOutlineFrom('junkers_kadlub');
        outliner.removeOutlineFrom('junkers_skrzydla');
        outliner.removeOutlineFrom('junkers_ogon');
        outliner.removeOutlineFrom('junkers_silniki');
        outliner.removeOutlineFrom('junkers_szklo');
        outliner.removeOutlineFrom('pulpit_bombowce');
        outliner.removeOutlineFrom('projektor_bombowce');
        outliner.removeOutlineFrom('projektor_bombowce_rolka_gora');
        outliner.removeOutlineFrom('projektor_bombowce_rolka_dol');
        outliner.removeOutlineFrom('podstawa_bombowce');
        outliner.removeOutlineFrom('projektor_bombowce_wlacznik');

        var menu_mysliwce = document.getElementById('menu_mysliwce');
        var menu_bombowce = document.getElementById('menu_bombowce');
        var menu_mysliwce_pulpit = document.getElementById('menu_mysliwce_pulpit');
        var menu_bombowce_pulpit = document.getElementById('menu_bombowce_pulpit');
        var menu_mysliwce_projektor = document.getElementById('menu_mysliwce_projektor');
        var menu_bombowce_projektor = document.getElementById('menu_bombowce_projektor');
        var home = document.getElementById('menu_home');

        menu_mysliwce.style.backgroundColor = 'white';
        menu_bombowce.style.backgroundColor = 'white';
        menu_mysliwce_pulpit.style.backgroundColor = 'white';
        menu_bombowce_pulpit.style.backgroundColor = 'white';
        menu_mysliwce_projektor.style.backgroundColor = 'white';
        menu_bombowce_projektor.style.backgroundColor = 'white';
        home.style.backgroundColor = 'white';
    }
))};