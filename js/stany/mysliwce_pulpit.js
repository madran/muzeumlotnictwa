function states_mysliwce_pulpit(states) {
    states.push(new TDU.WorldState('mysliwce_pulpit', function(s, fromButton) {
        world.controls = null;
        var pulpit_mysliwce = s.world.getObjectByName('pulpit_mysliwce');
        pulpit_mysliwce.getEvent('pulpit_mysliwce_najazd').isActive = false;
        pulpit_mysliwce.getEvent('pulpit_mysliwce_highlight').isActive = false;

        var zeszyt_otwarty_pulpit_mysliwce = s.world.getObjectByName('zeszyt_otwarty_pulpit_mysliwce');
        zeszyt_otwarty_pulpit_mysliwce.getEvent('pulpit_mysliwce_najazd').isActive = false;
        zeszyt_otwarty_pulpit_mysliwce.getEvent('zeszyt_otwarty_pulpit_mysliwce_highlight').isActive = false;

        var spitfire = world.getObjectByName('spitfire');
        spitfire.getEvent('spitfire_zoom').isActive = true;
        spitfire.getEvent('spitfire_highlight').isActive = true;

        var spitfire_szyba = s.world.getObjectByName('spitfire_szyba');
        spitfire_szyba.getEvent('spitfire_zoom').isActive = true;
        spitfire_szyba.getEvent('spitfire_szyba_highlight').isActive = true;

        var messerschmitt = world.getObjectByName('messerschmitt');
        messerschmitt.getEvent('messerschmitt_zoom').isActive = true;
        messerschmitt.getEvent('messerschmitt_highlight').isActive = true;

        var messerschmitt_szyba = s.world.getObjectByName('messerschmitt_szyba');
        messerschmitt_szyba.getEvent('messerschmitt_zoom').isActive = true;
        messerschmitt_szyba.getEvent('messerschmitt_szyba_highlight').isActive = true;

        outliner.setOutlineOn('spitfire');
        outliner.setOutlineOn('spitfire_szyba');
        outliner.setOutlineOn('messerschmitt');
        outliner.setOutlineOn('messerschmitt_szyba');

        outliner.removeOutlineFrom('pulpit_mysliwce');
        outliner.removeOutlineFrom('projektor_mysliwce_wlacznik');

        if (fromButton) {
            var tween_pos = new TWEEN.Tween(world.camera.position)
                    .to({x: -237.8, y: -48.6, z: -122.7}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
            var tween_rot = new TWEEN.Tween(world.camera.rotation)
                    .to({x: -0.2, y: 0.3, z: 0.1}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
        } else {
            world.camera.position.set(-237.8, -48.6, -122.7);
            world.camera.rotation.set(-0.2, 0.3, 0.1);
        }

        var messerschmitt = world.getObjectByName('messerschmitt');
        var messerschmitt_szyba = world.getObjectByName('messerschmitt_szyba');
        var spitfire = world.getObjectByName('spitfire');
        var spitfire_szyba = world.getObjectByName('spitfire_szyba');

        if (window.location.hash.split('/')[1] === 'gb') {
            spitfire.render();
            spitfire_szyba.render();

            messerschmitt.doNotRender();
            messerschmitt_szyba.doNotRender();
        }
        if (window.location.hash.split('/')[1] === 'ger') {
            spitfire.doNotRender();
            spitfire_szyba.doNotRender();

            messerschmitt.render();
            messerschmitt_szyba.render();
        }


        if (window.location.hash.split('/')[1] === 'gb') {
            mysliwce_wybor_flagi_ger.style.opacity = 0.25;
            mysliwce_wybor_flagi_gb.style.opacity = 1;

            mysliwce_wybor.removeChild(mysliwce_wybor.children[1]);
            mysliwce_wybor.appendChild(mysliwce_pulpit_samoloty_lista_gb);

            mysliwce_info.removeChild(mysliwce_info.firstElementChild);
            mysliwce_info.appendChild(table_spitfire);
        }
        if (window.location.hash.split('/')[1] === 'ger') {
            mysliwce_wybor_flagi_ger.style.opacity = 1;
            mysliwce_wybor_flagi_gb.style.opacity = 0.25;

            mysliwce_wybor.removeChild(mysliwce_wybor.children[1]);
            mysliwce_wybor.appendChild(mysliwce_pulpit_samoloty_lista_ger);

            mysliwce_info.removeChild(mysliwce_info.firstElementChild);
            mysliwce_info.appendChild(table_messerschmitt);
        }
//        }

        var menu_mysliwce = document.getElementById('menu_mysliwce');
        var menu_bombowce = document.getElementById('menu_bombowce');
        var menu_mysliwce_pulpit = document.getElementById('menu_mysliwce_pulpit');
        var menu_bombowce_pulpit = document.getElementById('menu_bombowce_pulpit');
        var menu_mysliwce_projektor = document.getElementById('menu_mysliwce_projektor');
        var menu_bombowce_projektor = document.getElementById('menu_bombowce_projektor');
        var home = document.getElementById('menu_home');

        menu_mysliwce.style.backgroundColor = 'white';
        menu_bombowce.style.backgroundColor = 'white';
        menu_mysliwce_pulpit.style.backgroundColor = 'black';
        menu_bombowce_pulpit.style.backgroundColor = 'white';
        menu_mysliwce_projektor.style.backgroundColor = 'white';
        menu_bombowce_projektor.style.backgroundColor = 'white';
        home.style.backgroundColor = 'white';
    }));
}