function states_mysliwce(state) {
    states.push(new TDU.WorldState('mysliwce', function(s, fromButton) {
        world.controls = null;
        var pulpit_mysliwce = s.world.getObjectByName('pulpit_mysliwce');
        pulpit_mysliwce.getEvent('pulpit_mysliwce_najazd').isActive = true;
        pulpit_mysliwce.getEvent('pulpit_mysliwce_highlight').isActive = true;

        var zeszyt_otwarty_pulpit_mysliwce = s.world.getObjectByName('zeszyt_otwarty_pulpit_mysliwce');
        zeszyt_otwarty_pulpit_mysliwce.getEvent('pulpit_mysliwce_najazd').isActive = true;
        zeszyt_otwarty_pulpit_mysliwce.getEvent('zeszyt_otwarty_pulpit_mysliwce_highlight').isActive = true;

        var projektor_mysliwce = s.world.getObjectByName('projektor_mysliwce');
        projektor_mysliwce.getEvent('najazd_na_ekran_mysliwce').isActive = true;
        projektor_mysliwce.getEvent('projektor_mysliwce_highlight').isActive = true;

        var projektor_mysliwce_rolka_gora = s.world.getObjectByName('projektor_mysliwce_rolka_gora');
        projektor_mysliwce_rolka_gora.getEvent('najazd_na_ekran_mysliwce').isActive = true;
        projektor_mysliwce_rolka_gora.getEvent('projektor_mysliwce_rolka_gora_highlight').isActive = true;

        var projektor_mysliwce_rolka_dol = s.world.getObjectByName('projektor_mysliwce_rolka_dol');
        projektor_mysliwce_rolka_dol.getEvent('najazd_na_ekran_mysliwce').isActive = true;
        projektor_mysliwce_rolka_dol.getEvent('projektor_mysliwce_rolka_dol_highlight').isActive = true;

        var podstawa_mysliwce = s.world.getObjectByName('podstawa_mysliwce');
        podstawa_mysliwce.getEvent('mysliwce_plan_ogolny').isActive = false;
        podstawa_mysliwce.getEvent('podstawa_mysliwce_highlight').isActive = false;

        var podstawa_napis_mysliwce = s.world.getObjectByName('podstawa_napis_mysliwce');
        podstawa_napis_mysliwce.getEvent('mysliwce_plan_ogolny').isActive = false;
        podstawa_napis_mysliwce.getEvent('podstawa_napis_mysliwce_highlight').isActive = false;

        var projektor_mysliwce_wlacznik = world.getObjectByName('projektor_mysliwce_wlacznik');
        projektor_mysliwce_wlacznik.getEvent('mysliwce_wlacz_film').isActive = false;
        projektor_mysliwce_wlacznik.getEvent('projektor_mysliwce_wlacznik_highlight').isActive = false;

        var spitfire = world.getObjectByName('spitfire');
        spitfire.getEvent('spitfire_zoom').isActive = true;
        spitfire.getEvent('spitfire_highlight').isActive = true;

        var spitfire_szyba = s.world.getObjectByName('spitfire_szyba');
        spitfire_szyba.getEvent('spitfire_zoom').isActive = true;
        spitfire_szyba.getEvent('spitfire_szyba_highlight').isActive = true;

        var messerschmitt = world.getObjectByName('messerschmitt');
        messerschmitt.getEvent('messerschmitt_zoom').isActive = true;
        messerschmitt.getEvent('messerschmitt_highlight').isActive = true;

        var messerschmitt_szyba = s.world.getObjectByName('messerschmitt_szyba');
        messerschmitt_szyba.getEvent('messerschmitt_zoom').isActive = true;
        messerschmitt_szyba.getEvent('messerschmitt_szyba_highlight').isActive = true;

        outliner.setOutlineOn('spitfire');
        outliner.setOutlineOn('spitfire_szyba');
        outliner.setOutlineOn('messerschmitt');
        outliner.setOutlineOn('messerschmitt_szyba');

        outliner.setOutlineOn('pulpit_mysliwce');
        outliner.setOutlineOn('projektor_mysliwce');
        outliner.setOutlineOn('projektor_mysliwce_rolka_gora');
        outliner.setOutlineOn('projektor_mysliwce_rolka_dol');
        outliner.removeOutlineFrom('podstawa_mysliwce');
        outliner2.removeOutlineFrom('projektor_mysliwce_wlacznik');

//        if (world.event === false) {
        if (fromButton) {
            var tween_pos = new TWEEN.Tween(world.camera.position)
                    .to({x: -56.9, y: 27.3, z: -12.2}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
            var tween_rot = new TWEEN.Tween(world.camera.rotation)
                    .to({x: -0.4, y: 0.5, z: 0.2}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
        } else {
            world.camera.position.set(-56.9, 27.3, -12.2);
            world.camera.rotation.set(-0.4, 0.5, 0.2);
        }
//        }
        if (spitfire_movie_container.children.length > 0) {
            if (window.location.hash.split('/')[1] === 'gb') {
                spitfire_movie_container.removeChild(spitfire_movie);
            }

            if (window.location.hash.split('/')[1] === 'ger') {
                spitfire_movie_container.removeChild(messerschmitt_movie);
            }
            projektor_mysliwce_wlacznik.on = false;
            rolka_dol.stop();
            rolka_gora.stop();
            projektor_mysliwce_rolka_dol.object.rotation.x = 2 * Math.PI;
            projektor_mysliwce_rolka_gora.object.rotation.x = 2 * Math.PI;

            var off_pos = projektor_mysliwce_wlacznik.object.rotation.y - 0.5

            projektor_mysliwce_wlacznik.object.rotation.y = off_pos;

            var outline = outliner2.getOutline('projektor_mysliwce_wlacznik');
            var mask = outliner2.getMask('projektor_mysliwce_wlacznik');

            outline.rotation.y = off_pos;
            mask.rotation.y = off_pos;
        }

        var messerschmitt = world.getObjectByName('messerschmitt');
        var messerschmitt_szyba = world.getObjectByName('messerschmitt_szyba');
        var spitfire = world.getObjectByName('spitfire');
        var spitfire_szyba = world.getObjectByName('spitfire_szyba');

        if (window.location.hash.split('/')[1] === 'gb') {
            spitfire.render();
            spitfire_szyba.render();

            messerschmitt.doNotRender();
            messerschmitt_szyba.doNotRender();
        }
        if (window.location.hash.split('/')[1] === 'ger') {
            spitfire.doNotRender();
            spitfire_szyba.doNotRender();

            messerschmitt.render();
            messerschmitt_szyba.render();
        }

        var menu_mysliwce = document.getElementById('menu_mysliwce');
        var menu_bombowce = document.getElementById('menu_bombowce');
        var menu_mysliwce_pulpit = document.getElementById('menu_mysliwce_pulpit');
        var menu_bombowce_pulpit = document.getElementById('menu_bombowce_pulpit');
        var menu_mysliwce_projektor = document.getElementById('menu_mysliwce_projektor');
        var menu_bombowce_projektor = document.getElementById('menu_bombowce_projektor');
        var home = document.getElementById('menu_home');
        
        menu_mysliwce.style.backgroundColor = 'black';
        menu_bombowce.style.backgroundColor = 'white';
        menu_mysliwce_pulpit.style.backgroundColor = 'white';
        menu_bombowce_pulpit.style.backgroundColor = 'white';
        menu_mysliwce_projektor.style.backgroundColor = 'white';
        menu_bombowce_projektor.style.backgroundColor = 'white';
        home.style.backgroundColor = 'white';
    }));
}