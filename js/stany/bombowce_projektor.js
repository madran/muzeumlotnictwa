function states_bombowce_projektor(states) {
    states.push(new TDU.WorldState('bombowce_projektor', function(s, fromButton) {
        world.controls = null;
        var pulpit_bombowce = s.world.getObjectByName('pulpit_bombowce');
        pulpit_bombowce.getEvent('pulpit_bombowce_najazd').isActive = false;

        var projektor_bombowce = s.world.getObjectByName('projektor_bombowce');
        projektor_bombowce.getEvent('najazd_na_ekran_bombowce').isActive = false;

        var projektor_bombowce = s.world.getObjectByName('projektor_bombowce');
        projektor_bombowce.getEvent('najazd_na_ekran_bombowce').isActive = false;

        var projektor_bombowce_rolka_gora = s.world.getObjectByName('projektor_bombowce_rolka_gora');
        projektor_bombowce_rolka_gora.getEvent('najazd_na_ekran_bombowce').isActive = false;

        var projektor_bombowce_rolka_dol = s.world.getObjectByName('projektor_bombowce_rolka_dol');
        projektor_bombowce_rolka_dol.getEvent('najazd_na_ekran_bombowce').isActive = false;

        var junkers_kadlub = world.getObjectByName('junkers_kadlub');
        junkers_kadlub.getEvent('junkers_kadlub_zoom').isActive = true
        junkers_kadlub.getEvent('junkers_kadlub_highlight').isActive = true
        var junkers_skrzydla = world.getObjectByName('junkers_skrzydla');
        junkers_skrzydla.getEvent('junkers_skrzydla_zoom').isActive = true
        junkers_skrzydla.getEvent('junkers_skrzydla_highlight').isActive = true
        var junkers_ogon = world.getObjectByName('junkers_ogon');
        junkers_ogon.getEvent('junkers_ogon_zoom').isActive = true
        junkers_ogon.getEvent('junkers_ogon_highlight').isActive = true
        var junkers_silniki = world.getObjectByName('junkers_silniki');
        junkers_silniki.getEvent('junkers_silniki_zoom').isActive = true
        junkers_silniki.getEvent('junkers_silniki_highlight').isActive = true
        var junkers_szklo = world.getObjectByName('junkers_szklo');
        junkers_szklo.getEvent('junkers_szklo_zoom').isActive = true
        junkers_szklo.getEvent('junkers_szklo_highlight').isActive = true


        outliner.removeOutlineFrom('pulpit_bombowce');
        outliner.removeOutlineFrom('projektor_bombowce');
        outliner.removeOutlineFrom('projektor_bombowce_rolka_gora');
        outliner.removeOutlineFrom('projektor_bombowce_rolka_dol');

        if (fromButton) {
            var tween_pos = new TWEEN.Tween(world.camera.position)
                    .to({x: 310.3, y: -46.8, z: 526.1}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
            var tween_rot = new TWEEN.Tween(world.camera.rotation)
                    .to({x: -0.2, y: -1.2, z: -0.2}, 2000)
                    .easing(TWEEN.Easing.Linear.None).start();
        } else {
            world.camera.position.set(310.3, -46.8, 526.1);
            world.camera.rotation.set(-0.2, -1.2, -0.2);
        }

        var messerschmitt = world.getObjectByName('messerschmitt');
        var messerschmitt_szyba = world.getObjectByName('messerschmitt_szyba');
        var spitfire = world.getObjectByName('spitfire');
        var spitfire_szyba = world.getObjectByName('spitfire_szyba');

        if (window.location.hash.split('/')[1] === 'gb') {
            spitfire.render();
            spitfire_szyba.render();

            messerschmitt.doNotRender();
            messerschmitt_szyba.doNotRender();
        }
        if (window.location.hash.split('/')[1] === 'ger') {
            spitfire.doNotRender();
            spitfire_szyba.doNotRender();

            messerschmitt.render();
            messerschmitt_szyba.render();
        }

        var menu_mysliwce = document.getElementById('menu_mysliwce');
        var menu_bombowce = document.getElementById('menu_bombowce');
        var menu_mysliwce_pulpit = document.getElementById('menu_mysliwce_pulpit');
        var menu_bombowce_pulpit = document.getElementById('menu_bombowce_pulpit');
        var menu_mysliwce_projektor = document.getElementById('menu_mysliwce_projektor');
        var menu_bombowce_projektor = document.getElementById('menu_bombowce_projektor');
        var home = document.getElementById('menu_home');

        menu_mysliwce.style.backgroundColor = 'white';
        menu_bombowce.style.backgroundColor = 'white';
        menu_mysliwce_pulpit.style.backgroundColor = 'white';
        menu_bombowce_pulpit.style.backgroundColor = 'white';
        menu_mysliwce_projektor.style.backgroundColor = 'white';
        menu_bombowce_projektor.style.backgroundColor = 'black';
        home.style.backgroundColor = 'white';
    }));
}