function setLights(result) {
    var lamp1 = world.getObjectByName('lampa1');
    var lamp2 = world.getObjectByName('lampa2');
    var lamp3 = world.getObjectByName('lampa3');
    var lamp4 = world.getObjectByName('lampa4');

    var shadow_light_color = 0xffffff;
    var shadow_light_intensity = 0.1;
    var shadow_light_distance = 0;
    var shadow_shadow_darkness = 0.1;
    var shadow_shadow_map = 4096;

    var shadow_camera_visible = false;

    var shadow_light1 = new world.gl.SpotLight(shadow_light_color, shadow_light_intensity, shadow_light_distance);
    shadow_light1.position = new world.gl.Vector3(20*29, (lamp1.object.position.y + 380), 10);
    shadow_light1.target.position.set(shadow_light1.position.x, 0, shadow_light1.position.z + 1);
    shadow_light1.angle = Math.PI;
    shadow_light1.castShadow = true;
    shadow_light1.shadowCameraVisible = shadow_camera_visible;
    shadow_light1.shadowMapWidth = shadow_shadow_map;
    shadow_light1.shadowMapHeight = shadow_shadow_map;
    shadow_light1.shadowDarkness = shadow_shadow_darkness;
    shadow_light1.shadowCameraNear = 500;
    shadow_light1.shadowCameraFar = 700;
    shadow_light1.shadowCameraFov = 80;

    var shadow_light2 = new world.gl.SpotLight(shadow_light_color, shadow_light_intensity, shadow_light_distance);
    shadow_light2.position = new world.gl.Vector3(lamp2.object.position.x, lamp2.object.position.y + 190, lamp2.object.position.z);
    shadow_light2.target.position.set(shadow_light2.position.x, 0, shadow_light2.position.z);
    shadow_light2.castShadow = true;
    shadow_light2.shadowCameraVisible = shadow_camera_visible;
    shadow_light2.shadowMapWidth = shadow_shadow_map;
    shadow_light2.shadowMapHeight = shadow_shadow_map;
    shadow_light2.shadowDarkness = shadow_shadow_darkness;
    shadow_light2.shadowCameraNear = 350;
    shadow_light2.shadowCameraFar = 500;
    shadow_light2.shadowCameraFov = 80;

    result.scene.add(shadow_light1);
    result.scene.add(shadow_light2);



    var point_light_intensity = 0.30;
    var point_light_color = 0xffffff;

    var point_light1 = new world.gl.PointLight(point_light_color, point_light_intensity);
    point_light1.position.copy(lamp1.object.position);

    var point_light2 = new world.gl.PointLight(point_light_color, point_light_intensity);
    point_light2.position.copy(lamp2.object.position);

    var point_light3 = new world.gl.PointLight(point_light_color, point_light_intensity);
    point_light3.position.copy(lamp3.object.position);

    var point_light4 = new world.gl.PointLight(point_light_color, point_light_intensity);
    point_light4.position.copy(lamp4.object.position);

    result.scene.add(point_light1);
    result.scene.add(point_light2);
    result.scene.add(point_light3);
    result.scene.add(point_light4);
}