var application = new TDU.Application(THREE, {
    events: true
});

var world = new TDU.World();
world.init = function() {
    this.container = document.body;

    window.onhashchange = function(e) {
        var state_name = window.location.hash.slice(1).split('/')[0];
        world.worldState.setActiveState(state_name, true);
    };

    var width = window.innerWidth;
    var height = window.innerHeight;

    this.scene = new this.gl.Scene();
    this.cssScene = new THREE.Scene();

    this.camera = new this.gl.PerspectiveCamera(45, width / height, 0.1, 10000);
    this.camera.position.set(-563.2, 205.6, 508.5);
    this.camera.rotation.set(-0.5, -0.6, -0.3);

    this.renderer = new this.gl.WebGLRenderer({antialias: true});
    this.renderer.setSize(width, height);
    this.renderer.autoClear = false;
    this.renderer.shadowMapEnabled = true;
    this.renderer.domElement.style.position = 'absolute';
    this.renderer.domElement.style.top = 0;
    this.renderer.domElement.style.zIndex = 0;

    this.cssRenderer3d = new this.gl.CSS3DRenderer();
    this.cssRenderer3d.setSize(width, height);
    this.cssRenderer3d.domElement.style.position = 'absolute';
    this.cssRenderer3d.domElement.style.top = 0;
    this.cssRenderer3d.domElement.style.zIndex = 1;

    this.container.appendChild(this.cssRenderer3d.domElement);
    this.container.appendChild(this.renderer.domElement);



    var loader = new this.gl.SceneLoader();
    loader.callbackProgress = callbackProgress;
    loader.load("../models/muzeum/muzeum.js", world.worldState.init);
    
    

//    var stats = new Stats();
//    stats.domElement.style.position = 'absolute';
//    stats.domElement.style.bottom = '0px';
//    stats.domElement.style.zIndex = 100;
//    this.container.appendChild(stats.domElement);
    
//   this.stats = stats;

    window.addEventListener('resize', onWindowResize, false);

};

function onWindowResize() {

    world.camera.aspect = window.innerWidth / window.innerHeight;
    world.camera.updateProjectionMatrix();
    
    
    var menu = document.getElementById('menu');
    var translate = 'translate(' + ((window.innerWidth / 2) - 150) + 'px, ' + (window.innerHeight - 20) + 'px)';

    menu.style.WebkitTransform = translate;
    menu.style.MozTransform = translate;
    menu.style.oTransform = translate;
    menu.style.transform = translate;

    world.renderer.setSize(window.innerWidth, window.innerHeight);
    world.cssRenderer3d.setSize(window.innerWidth, window.innerHeight);

}


world.update = function() {
    TWEEN.update();
//    world.stats.update();
    if (world.controls) {
        world.controls.update();
    }
};

world.worldState.setInitialState(function(result) {
    Object.keys(result.objects).forEach(function(obj_name) {
        TDUObject = new TDU.Object(obj_name, world);
        TDUObject.object = result.objects[obj_name];
        world.addObject(TDUObject);
    });

    if (window.location.hash.length === 0) {
        window.location.hash = 'home/gb/Spitfire';
    }
    
    setLights(result);
    setObjects();

    world.worldState.states = states();

    world.scene = result.scene;

    $('#page').remove();

    application.run();
});

if (!Detector.webgl)
    Detector.addGetWebGLMessage();
else {
    var outliner = new TDU.ObjectOutlinerPlugin(world, 2, 'yellow');
    var outliner2 = new TDU.ObjectOutlinerPlugin(world, 2, 'yellow');
    application.addPostPlugin(outliner);
    application.addPostPlugin(outliner2);
    application.setWorld(world);
}

function callbackProgress(progress, result) {
    var width = 700;

    var total = progress.totalModels + progress.totalTextures;
    var loaded = progress.loadedModels + progress.loadedTextures;


    if (total > 0)
        width = Math.floor(width * loaded / total);


    $('#progressBar').width(width);
}

function mysliwce_zoom_camera() {
    world.controls = new world.gl.OrbitControls(world.camera);
    world.controls.userPan = false;
    world.controls.center = new THREE.Vector3(-414.38, -41.3194, -335.882);
    world.controls.maxDistance = 300;
    world.controls.minDistance = 100;
    world.controls.maxPolarAngle = Math.PI / 2;
    world.controls.userZoomSpeed = 0.5;
}

function bombowce_zoom_camera() {
    world.controls = new world.gl.OrbitControls(world.camera);
    world.controls.userPan = false;
    world.controls.center = new THREE.Vector3(547.583, -10.8103, -55.0442);
    world.controls.maxDistance = 300;
    world.controls.minDistance = 100;
    world.controls.maxPolarAngle = Math.PI / 2;
    world.controls.userZoomSpeed = 0.5;
}