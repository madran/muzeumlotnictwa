function states() {
    states = [];
    
    states_home(states);

    states_mysliwce(states);
    states_mysliwce_projektor(states);
    states_mysliwce_pulpit(states);
    states_mysliwce_pulpit_info(states);
    states_mysliwce_zoom(states);

    states_bombowce(states);
    states_bombowce_projektor(states);
    states_bombowce_pulpit(states);
    states_bombowce_zoom(states);

    return states;
}