function objects_menu() {
    var menu = document.getElementById('menu');
    menu.style.width = '300px';
    menu.style.height = '100px';
    menu.style.position = 'absolute';
    menu.style.zIndex = 3;

    var translate = 'translate(' + ((window.innerWidth / 2) - 150) + 'px, ' + (window.innerHeight - 20) + 'px)';

    menu.style.WebkitTransform = translate;
    menu.style.MozTransform = translate;
    menu.style.oTransform = translate;
    menu.style.transform = translate;

    document.body.appendChild(menu);


    var menu_top = document.getElementById('menu_top');



    var menu_offset = {y: 20};
    menu_top.addEventListener('click', function() {
        if (menu.style.transform)
            var base = menu.style.transform.substring(translate.indexOf('translate('), translate.indexOf(')'));
        else if (menu.style.WebkitTransform)
            var base = menu.style.WebkitTransform.substring(translate.indexOf('translate('), translate.indexOf(')'));

        var coords = {}
        coords.X = parseFloat(base.split('translate(')[1]);
        coords.Y = parseFloat(base.split(', ')[1]);

        if (coords.Y === window.innerHeight - 20) {
            var tween = new TWEEN.Tween(menu_offset).to({y: 100}, 250).easing(TWEEN.Easing.Linear.None).onUpdate(function() {
                var translate = 'translate(' + ((window.innerWidth / 2) - 150) + 'px, ' + (window.innerHeight - menu_offset.y) + 'px)';
                menu.style.WebkitTransform = translate;
                menu.style.MozTransform = translate;
                menu.style.oTransform = translate;
                menu.style.transform = translate;
            }).start();
        } else {
            var tween = new TWEEN.Tween(menu_offset).to({y: 20}, 250).easing(TWEEN.Easing.Linear.None).onUpdate(function() {
                var translate = 'translate(' + ((window.innerWidth / 2) - 150) + 'px, ' + (window.innerHeight - menu_offset.y) + 'px)';
                menu.style.WebkitTransform = translate;
                menu.style.MozTransform = translate;
                menu.style.oTransform = translate;
                menu.style.transform = translate;
            }).start();
        }
    }, false);

    var menu_mysliwce = document.getElementById('menu_mysliwce');
    menu_mysliwce.addEventListener('click', function() {
        if (window.location.hash.slice(1).split('/')[0] !== 'mysliwce')
            world.worldState.setActiveState('mysliwce', false);
    }, false);

    var menu_bombowce = document.getElementById('menu_bombowce');
    menu_bombowce.addEventListener('click', function() {
        if (window.location.hash.slice(1).split('/')[0] !== 'bombowce')
            world.worldState.setActiveState('bombowce', false);

    }, false);

    var menu_mysliwce_pulpit = document.getElementById('menu_mysliwce_pulpit');
    menu_mysliwce_pulpit.addEventListener('click', function() {
        if (window.location.hash.slice(1).split('/')[0] !== 'mysliwce_pulpit')
            world.worldState.setActiveState('mysliwce_pulpit', false);
    }, false);

    var menu_bombowce_pulpit = document.getElementById('menu_bombowce_pulpit');
    menu_bombowce_pulpit.addEventListener('click', function() {
        if (window.location.hash.slice(1).split('/')[0] !== 'bombowce_pulpit')
            world.worldState.setActiveState('bombowce_pulpit', false);
    }, false);

    var menu_mysliwce_projektor = document.getElementById('menu_mysliwce_projektor');
    menu_mysliwce_projektor.addEventListener('click', function() {
        if (window.location.hash.slice(1).split('/')[0] !== 'mysliwce_projektor')
            world.worldState.setActiveState('mysliwce_projektor', false);
    }, false);

    var menu_bombowce_projektor = document.getElementById('menu_bombowce_projektor');
    menu_bombowce_projektor.addEventListener('click', function() {
        if (window.location.hash.slice(1).split('/')[0] !== 'bombowce_projektor')
            world.worldState.setActiveState('bombowce_projektor', false);
    }, false);
    
    var home = document.getElementById('menu_home');
    home.addEventListener('click', function() {
        if (window.location.hash.slice(1).split('/')[0] !== 'home')
            world.worldState.setActiveState('home', false);
    }, false);
}