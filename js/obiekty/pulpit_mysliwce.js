function objects_pulpit_mysliwce() {
    var pulpit_mysliwce = world.getObjectByName('pulpit_mysliwce');
    var zeszyt_otwarty_pulpit_mysliwce = world.getObjectByName('zeszyt_otwarty_pulpit_mysliwce');

    pulpit_mysliwce.object.material.emissive = new THREE.Color(0x888888);
    zeszyt_otwarty_pulpit_mysliwce.object.material.emissive = new THREE.Color(0x888888);

    pulpit_mysliwce.addEvent(new TDU.Event('pulpit_mysliwce_najazd', 'click', function() {
        world.worldState.setActiveState('mysliwce_pulpit');
    }));

    pulpit_mysliwce.addEvent(new TDU.Event('pulpit_mysliwce_highlight', 'mousemove', function() {
        pulpit_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        zeszyt_otwarty_pulpit_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            pulpit_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            zeszyt_otwarty_pulpit_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    outliner.addObject(pulpit_mysliwce);


    zeszyt_otwarty_pulpit_mysliwce.addEvent(new TDU.Event('pulpit_mysliwce_najazd', 'click', function() {
        world.worldState.setActiveState('mysliwce_pulpit');
    }));

    zeszyt_otwarty_pulpit_mysliwce.addEvent(new TDU.Event('zeszyt_otwarty_pulpit_mysliwce_highlight', 'mousemove', function() {
        pulpit_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        zeszyt_otwarty_pulpit_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            pulpit_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            zeszyt_otwarty_pulpit_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));



    var zeszyt_mysliwce_plane = world.getObjectByName('zeszyt_mysliwce_plane');
    zeszyt_mysliwce_plane.object.visible = false;
}