function objects_projektor_mysliwce() {
    var projektor_mysliwce = world.getObjectByName('projektor_mysliwce');
    var projektor_mysliwce_rolka_gora = world.getObjectByName('projektor_mysliwce_rolka_gora');
    var projektor_mysliwce_rolka_dol = world.getObjectByName('projektor_mysliwce_rolka_dol');

    projektor_mysliwce.object.material.emissive = new THREE.Color(0x888888);
    projektor_mysliwce_rolka_gora.object.material.emissive = new THREE.Color(0x888888);
    projektor_mysliwce_rolka_dol.object.material.emissive = new THREE.Color(0x888888);

    projektor_mysliwce.addEvent(new TDU.Event('najazd_na_ekran_mysliwce', 'click', function() {
        world.worldState.setActiveState('mysliwce_projektor');
    }));

    projektor_mysliwce.addEvent(new TDU.Event('projektor_mysliwce_highlight', 'mousemove', function() {
        projektor_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        projektor_mysliwce_rolka_gora.object.material.emissive = new THREE.Color(0xffffff);
        projektor_mysliwce_rolka_dol.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            projektor_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            projektor_mysliwce_rolka_gora.object.material.emissive = new THREE.Color(0x888888);
            projektor_mysliwce_rolka_dol.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    outliner.addObject(projektor_mysliwce);





    projektor_mysliwce_rolka_gora.addEvent(new TDU.Event('najazd_na_ekran_mysliwce', 'click', function() {
        world.worldState.setActiveState('mysliwce_projektor');
    }));

    projektor_mysliwce_rolka_gora.addEvent(new TDU.Event('projektor_mysliwce_rolka_gora_highlight', 'mousemove', function() {
        projektor_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        projektor_mysliwce_rolka_gora.object.material.emissive = new THREE.Color(0xffffff);
        projektor_mysliwce_rolka_dol.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            projektor_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            projektor_mysliwce_rolka_gora.object.material.emissive = new THREE.Color(0x888888);
            projektor_mysliwce_rolka_dol.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }, true));

    outliner.addObject(projektor_mysliwce_rolka_gora);





    projektor_mysliwce_rolka_dol.addEvent(new TDU.Event('najazd_na_ekran_mysliwce', 'click', function() {
        world.worldState.setActiveState('mysliwce_projektor');
    }));

    projektor_mysliwce_rolka_dol.addEvent(new TDU.Event('projektor_mysliwce_rolka_dol_highlight', 'mousemove', function() {
        projektor_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        projektor_mysliwce_rolka_gora.object.material.emissive = new THREE.Color(0xffffff);
        projektor_mysliwce_rolka_dol.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            projektor_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            projektor_mysliwce_rolka_gora.object.material.emissive = new THREE.Color(0x888888);
            projektor_mysliwce_rolka_dol.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    outliner.addObject(projektor_mysliwce_rolka_dol);



    var projektor_mysliwce_wlacznik = world.getObjectByName('projektor_mysliwce_wlacznik');

    rolka_dol = new TWEEN.Tween(projektor_mysliwce_rolka_dol.object.rotation)
            .to({x: -(2 * Math.PI), y: projektor_mysliwce_rolka_dol.object.rotation.y, z: projektor_mysliwce_rolka_dol.object.rotation.z}, 2000)
            .repeat(Infinity)
            .easing(TWEEN.Easing.Linear.None);

    rolka_gora = new TWEEN.Tween(projektor_mysliwce_rolka_gora.object.rotation)
            .to({x: -(2 * Math.PI), y: projektor_mysliwce_rolka_gora.object.rotation.y, z: projektor_mysliwce_rolka_gora.object.rotation.z}, 2000)
            .repeat(Infinity)
            .easing(TWEEN.Easing.Linear.None);
    i = 1;
    projektor_mysliwce_wlacznik.addEvent(new TDU.Event('mysliwce_wlacz_film', 'click', function() {
        if (projektor_mysliwce_wlacznik.on === false) {
            var on = new TWEEN.Tween(projektor_mysliwce_wlacznik.object.rotation)
                    .to({x: projektor_mysliwce_wlacznik.object.rotation.x, y: projektor_mysliwce_wlacznik.object.rotation.y + 0.5, z: projektor_mysliwce_wlacznik.object.rotation.z}, 50)
                    .easing(TWEEN.Easing.Cubic.In).start();

            var outline = outliner2.getOutline('projektor_mysliwce_wlacznik');
            var mask = outliner2.getMask('projektor_mysliwce_wlacznik');

            var on_outline = new TWEEN.Tween(outline.rotation)
                    .to({x: outline.rotation.x, y: outline.rotation.y + 0.5, z: outline.rotation.z}, 50)
                    .easing(TWEEN.Easing.Cubic.In).start();
            var on_mask = new TWEEN.Tween(mask.rotation)
                    .to({x: mask.rotation.x, y: mask.rotation.y + 0.5, z: mask.rotation.z}, 50)
                    .easing(TWEEN.Easing.Cubic.In).onComplete(function() {
                rolka_dol.start();
                rolka_gora.start();
                if (window.location.hash.split('/')[1] === 'gb') {
                    spitfire_movie_container.appendChild(spitfire_movie);
                }

                if (window.location.hash.split('/')[1] === 'ger') {
                    spitfire_movie_container.appendChild(messerschmitt_movie);
                }
            }).start();
            projektor_mysliwce_wlacznik.on = true;

        } else {
            var on = new TWEEN.Tween(projektor_mysliwce_wlacznik.object.rotation)
                    .to({x: projektor_mysliwce_wlacznik.object.rotation.x, y: projektor_mysliwce_wlacznik.object.rotation.y - 0.5, z: projektor_mysliwce_wlacznik.object.rotation.z}, 50)
                    .easing(TWEEN.Easing.Cubic.In).start();

            var outline = outliner2.getOutline('projektor_mysliwce_wlacznik');
            var mask = outliner2.getMask('projektor_mysliwce_wlacznik');

            var on_outline = new TWEEN.Tween(outline.rotation)
                    .to({x: outline.rotation.x, y: outline.rotation.y - 0.5, z: outline.rotation.z}, 50)
                    .easing(TWEEN.Easing.Cubic.In).start();
            var on_mask = new TWEEN.Tween(mask.rotation)
                    .to({x: mask.rotation.x, y: mask.rotation.y - 0.5, z: mask.rotation.z}, 50)
                    .easing(TWEEN.Easing.Cubic.In).onComplete(function() {
                rolka_dol.stop();
                rolka_gora.stop();
                projektor_mysliwce_rolka_dol.object.rotation.x = 2 * Math.PI;
                projektor_mysliwce_rolka_gora.object.rotation.x = 2 * Math.PI;
                if (window.location.hash.split('/')[1] === 'gb') {
                    spitfire_movie_container.removeChild(spitfire_movie);
                }

                if (window.location.hash.split('/')[1] === 'ger') {
                    spitfire_movie_container.removeChild(messerschmitt_movie);
                }
                on_mask.stop();
            }).start();
            projektor_mysliwce_wlacznik.on = false;
        }
    }));

    projektor_mysliwce_wlacznik.addEvent(new TDU.Event('projektor_mysliwce_wlacznik_highlight', 'mousemove', function() {
        projektor_mysliwce_wlacznik.object.material.emissive = new THREE.Color(0xaaaaaa);
        document.body.style.cursor = "pointer";
        return function() {
            projektor_mysliwce_wlacznik.object.material.emissive = new THREE.Color(0x000000);
            document.body.style.cursor = "default";
        }
    }));

    outliner2.addObject(projektor_mysliwce_wlacznik);
}