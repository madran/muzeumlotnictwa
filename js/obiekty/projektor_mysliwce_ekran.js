function objects_projektor_mysliwce_ekran() {
    spitfire_movie_container = document.createElement('div');
    spitfire_movie_container.style.width = '640px';
    spitfire_movie_container.style.height = '360px';
    spitfire_movie_container.id = 'spitfire_movie_container';

    spitfire_movie = document.getElementById('spitfire_movie');
    spitfire_movie.parentNode.removeChild(spitfire_movie);

    messerschmitt_movie = document.getElementById('messerschmitt_movie');
    messerschmitt_movie.parentNode.removeChild(messerschmitt_movie);


    var projektor_mysliwce_wlacznik = world.getObjectByName('projektor_mysliwce_wlacznik');
    projektor_mysliwce_wlacznik.on = false;

    var ekran_mysliwce = world.getObjectByName('ekran_mysliwce');
    var objectCSS = new THREE.CSS3DObject(spitfire_movie_container);
    objectCSS.name = 'ekran_mysliwce';
    objectCSS.position = ekran_mysliwce.object.position;

    var scale = 0.1;
    objectCSS.scale.x = scale;
    objectCSS.scale.y = scale;
    objectCSS.scale.z = scale;

    world.cssScene.add(objectCSS);
}