function objects_projektor_bombowce() {
    var projektor_bombowce = world.getObjectByName('projektor_bombowce');
    var projektor_bombowce_rolka_gora = world.getObjectByName('projektor_bombowce_rolka_gora');
    var projektor_bombowce_rolka_dol = world.getObjectByName('projektor_bombowce_rolka_dol');

    projektor_bombowce.object.material.emissive = new THREE.Color(0x444444);
    projektor_bombowce_rolka_gora.object.material.emissive = new THREE.Color(0x444444);
    projektor_bombowce_rolka_dol.object.material.emissive = new THREE.Color(0x444444);

    projektor_bombowce.addEvent(new TDU.Event('najazd_na_ekran_bombowce', 'click', function() {
        world.worldState.setActiveState('bombowce_projektor');
    }));

    projektor_bombowce.addEvent(new TDU.Event('projektor_bombowce_highlight', 'mousemove', function() {
        projektor_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        projektor_bombowce_rolka_gora.object.material.emissive = new THREE.Color(0xffffff);
        projektor_bombowce_rolka_dol.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            projektor_bombowce.object.material.emissive = new THREE.Color(0x444444);
            projektor_bombowce_rolka_gora.object.material.emissive = new THREE.Color(0x444444);
            projektor_bombowce_rolka_dol.object.material.emissive = new THREE.Color(0x444444);
            document.body.style.cursor = "default";
        }
    }));

    outliner.addObject(projektor_bombowce);


    projektor_bombowce_rolka_gora.addEvent(new TDU.Event('najazd_na_ekran_bombowce', 'click', function() {
        world.worldState.setActiveState('bombowce_projektor');
    }));

    projektor_bombowce_rolka_gora.addEvent(new TDU.Event('projektor_bombowce_rolka_gora_highlight', 'mousemove', function() {
        projektor_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        projektor_bombowce_rolka_gora.object.material.emissive = new THREE.Color(0xffffff);
        projektor_bombowce_rolka_dol.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            projektor_bombowce.object.material.emissive = new THREE.Color(0x444444);
            projektor_bombowce_rolka_gora.object.material.emissive = new THREE.Color(0x444444);
            projektor_bombowce_rolka_dol.object.material.emissive = new THREE.Color(0x444444);
            document.body.style.cursor = "default";
        }
    }));

    outliner.addObject(projektor_bombowce_rolka_gora);


    projektor_bombowce_rolka_dol.addEvent(new TDU.Event('najazd_na_ekran_bombowce', 'click', function() {
        world.worldState.setActiveState('bombowce_projektor');
    }));

    projektor_bombowce_rolka_dol.addEvent(new TDU.Event('projektor_bombowce_rolka_dol_highlight', 'mousemove', function() {
        projektor_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        projektor_bombowce_rolka_gora.object.material.emissive = new THREE.Color(0xffffff);
        projektor_bombowce_rolka_dol.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            projektor_bombowce.object.material.emissive = new THREE.Color(0x444444);
            projektor_bombowce_rolka_gora.object.material.emissive = new THREE.Color(0x444444);
            projektor_bombowce_rolka_dol.object.material.emissive = new THREE.Color(0x444444);
            document.body.style.cursor = "default";
        }
    }));

    outliner.addObject(projektor_bombowce_rolka_dol);
}