function objects_messerschmitt() {
    var messerschmitt = world.getObjectByName('messerschmitt');
    var messerschmitt_szyba = world.getObjectByName('messerschmitt_szyba');

    outliner.addObject(messerschmitt, true);
    outliner.addObject(messerschmitt_szyba, true);

    messerschmitt.object.material.emissive = new THREE.Color(0x888888);
    messerschmitt_szyba.object.material.emissive = new THREE.Color(0x888888);

    messerschmitt.addEvent(new TDU.Event('messerschmitt_zoom', 'click', function() {
        world.worldState.setActiveState('mysliwce_zoom');
    }, true));

    messerschmitt_szyba.addEvent(new TDU.Event('messerschmitt_zoom', 'click', function() {
        world.worldState.setActiveState('mysliwce_zoom');
    }, true));

    messerschmitt.addEvent(new TDU.Event('messerschmitt_highlight', 'mousemove', function() {
        messerschmitt.object.material.emissive = new THREE.Color(0xffffff);
        messerschmitt_szyba.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            messerschmitt.object.material.emissive = new THREE.Color(0x888888);
            messerschmitt_szyba.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));


    messerschmitt_szyba.addEvent(new TDU.Event('messerschmitt_szyba_highlight', 'mousemove', function() {
        messerschmitt.object.material.emissive = new THREE.Color(0xffffff);
        messerschmitt_szyba.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            messerschmitt.object.material.emissive = new THREE.Color(0x000000);
            messerschmitt_szyba.object.material.emissive = new THREE.Color(0x000000);
            document.body.style.cursor = "default";
        }
    }));

    if (window.location.hash.split('/')[2] === 'Spitfire') {
        messerschmitt.doNotRender();
        messerschmitt_szyba.doNotRender();
    }
}