function objects_pulpit_mysliwce_zeszyt() {
    mysliwce_wybor = document.getElementById('mysliwce_wybor');
    mysliwce_info = document.getElementById('mysliwce_info');

    mysliwce_wybor_flagi_ger = document.getElementById('mysliwce_wybor_flagi_ger');
    mysliwce_wybor_flagi_gb = document.getElementById('mysliwce_wybor_flagi_gb');
    mysliwce_pulpit_samoloty_lista_gb = document.getElementById('mysliwce_pulpit_samoloty_lista_gb');
    mysliwce_pulpit_samoloty_lista_ger = document.getElementById('mysliwce_pulpit_samoloty_lista_ger');
    table_spitfire = document.getElementById('table_spitfire');
    table_messerschmitt = document.getElementById('table_messerschmitt');
    spitfire_info = document.getElementById('spitfire_info');
    messerschmitt_info = document.getElementById('messerschmitt_info');


    mysliwce_pulpit_samoloty_lista_ger.parentNode.removeChild(mysliwce_pulpit_samoloty_lista_ger);
    mysliwce_pulpit_samoloty_lista_gb.parentNode.removeChild(mysliwce_pulpit_samoloty_lista_gb);
    table_spitfire.parentNode.removeChild(table_spitfire);
    table_messerschmitt.parentNode.removeChild(table_messerschmitt);
    
    if (window.location.hash.split('/')[1] === 'gb') {
        mysliwce_wybor_flagi_ger.style.opacity = 0.25;
        mysliwce_wybor_flagi_gb.style.opacity = 1;
        mysliwce_wybor.appendChild(mysliwce_pulpit_samoloty_lista_gb);
        mysliwce_info.appendChild(table_spitfire);
    }
    if (window.location.hash.split('/')[1] === 'ger') {
        mysliwce_wybor_flagi_ger.style.opacity = 1;
        mysliwce_wybor_flagi_gb.style.opacity = 0.25;
        mysliwce_wybor.appendChild(mysliwce_pulpit_samoloty_lista_ger);
        mysliwce_info.appendChild(table_messerschmitt);
    }


    mysliwce_ksiazka_pulpit = document.getElementById('mysliwce_ksiazka_pulpit');
    mysliwce_ksiazka_pulpit.parentNode.removeChild(mysliwce_ksiazka_pulpit);

    var messerschmitt = world.getObjectByName('messerschmitt');
    var messerschmitt_szyba = world.getObjectByName('messerschmitt_szyba');
    var spitfire = world.getObjectByName('spitfire');
    var spitfire_szyba = world.getObjectByName('spitfire_szyba');

    mysliwce_wybor_flagi_gb.addEventListener('click', function() {
        mysliwce_wybor_flagi_ger.style.opacity = 0.25;
        mysliwce_wybor_flagi_gb.style.opacity = 1;

        mysliwce_wybor.removeChild(mysliwce_wybor.children[1]);
        mysliwce_wybor.appendChild(mysliwce_pulpit_samoloty_lista_gb);

        mysliwce_info.removeChild(mysliwce_info.firstElementChild);
        mysliwce_info.appendChild(table_spitfire);
        spitfire.render();
        spitfire_szyba.render();

        messerschmitt.doNotRender();
        messerschmitt_szyba.doNotRender();

        var name = window.location.hash.split('/')[0];
        if (document.location.hash !== name + '/' + 'gb' + '/' + 'Spitfire') {
            history.pushState({}, name, 'index.html' + name + '/' + 'gb' + '/' + 'Spitfire');
        }
    }, false);

    mysliwce_wybor_flagi_ger.addEventListener('click', function() {
        mysliwce_wybor_flagi_ger.style.opacity = 1;
        mysliwce_wybor_flagi_gb.style.opacity = 0.25;

        mysliwce_wybor.removeChild(mysliwce_wybor.children[1]);
        mysliwce_wybor.appendChild(mysliwce_pulpit_samoloty_lista_ger);

        mysliwce_info.removeChild(mysliwce_info.firstElementChild);
        mysliwce_info.appendChild(table_messerschmitt);

        spitfire.doNotRender();
        spitfire_szyba.doNotRender();

        messerschmitt.render();
        messerschmitt_szyba.render();

        var name = window.location.hash.split('/')[0];
        if (document.location.hash !== name + '/' + 'ger' + '/' + 'MesserschmittBf109') {
            history.pushState({}, name, 'index.html' + name + '/' + 'ger' + '/' + 'MesserschmittBf109');
        }
    }, false);


    spitfire_info.addEventListener('click', function() {
        if (world.worldState.activeState.name !== 'mysliwce_pulpit_info') {
            world.worldState.setActiveState('mysliwce_pulpit_info');
        }
    }, false);

    messerschmitt_info.addEventListener('click', function() {
        if (world.worldState.activeState.name !== 'mysliwce_pulpit_info') {
            world.worldState.setActiveState('mysliwce_pulpit_info');
        }
    }, false);



    var zeszyt_mysliwce_plane = world.getObjectByName('zeszyt_mysliwce_plane');
    var objectCSS = new THREE.CSS3DObject(mysliwce_ksiazka_pulpit);
    objectCSS.name = 'pulpit_mysliwce_plane';
    objectCSS.position = zeszyt_mysliwce_plane.object.position;
    objectCSS.rotation.x = -0.87;

    var scale = 0.03;
    objectCSS.scale.x = scale;
    objectCSS.scale.y = scale;
    objectCSS.scale.z = scale;

    world.cssScene.add(objectCSS);
}