function objects_spitfire() {
    var spitfire = world.getObjectByName('spitfire');
    var spitfire_szyba = world.getObjectByName('spitfire_szyba');
    outliner.addObject(spitfire, true);
    outliner.addObject(spitfire_szyba, true);

    spitfire.object.material.emissive = new THREE.Color(0x888888);
    spitfire_szyba.object.material.emissive = new THREE.Color(0x888888);

    spitfire.addEvent(new TDU.Event('spitfire_zoom', 'click', function() {
        world.worldState.setActiveState('mysliwce_zoom');
    }, true));

    spitfire_szyba.addEvent(new TDU.Event('spitfire_zoom', 'click', function() {
        world.worldState.setActiveState('mysliwce_zoom');
    }, true));

    if (window.location.hash.split('/')[2] === 'MesserschmittBf109') {
        spitfire.doNotRender();
        spitfire_szyba.doNotRender();
    }

    spitfire.addEvent(new TDU.Event('spitfire_highlight', 'mousemove', function() {
        spitfire.object.material.emissive = new THREE.Color(0xffffff);
        spitfire_szyba.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            spitfire.object.material.emissive = new THREE.Color(0x888888);
            spitfire_szyba.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));


    spitfire_szyba.addEvent(new TDU.Event('spitfire_szyba_highlight', 'mousemove', function() {
        spitfire.object.material.emissive = new THREE.Color(0xffffff);
        spitfire_szyba.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            spitfire.object.material.emissive = new THREE.Color(0x888888);
            spitfire_szyba.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));
}