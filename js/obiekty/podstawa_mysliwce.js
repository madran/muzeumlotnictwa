function objects_podstawa_mysliwce() {
    var podstawa_mysliwce = world.getObjectByName('podstawa_mysliwce');
    var podstawa_napis_mysliwce = world.getObjectByName('podstawa_napis_mysliwce');

    podstawa_mysliwce.object.material.emissive = new THREE.Color(0x888888);
    podstawa_napis_mysliwce.object.material.emissive = new THREE.Color(0x888888);

    podstawa_mysliwce.addEvent(new TDU.Event('podstawa_mysliwce_highlight', 'mousemove', function() {
        podstawa_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        podstawa_napis_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            podstawa_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            podstawa_napis_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }, true));

    podstawa_mysliwce.addEvent(new TDU.Event('mysliwce_plan_ogolny', 'click', function() {
        world.worldState.setActiveState('mysliwce');
    }, true));

    outliner.addObject(podstawa_mysliwce, true);




    podstawa_napis_mysliwce.addEvent(new TDU.Event('mysliwce_plan_ogolny', 'click', function() {
        world.worldState.setActiveState('mysliwce');
    }, true));

    podstawa_napis_mysliwce.addEvent(new TDU.Event('podstawa_napis_mysliwce_highlight', 'mousemove', function() {
        podstawa_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        podstawa_napis_mysliwce.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            podstawa_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            podstawa_napis_mysliwce.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }, true));
}