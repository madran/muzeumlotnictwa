function objects_podstawa_bombowce() {
    var podstawa_bombowce = world.getObjectByName('podstawa_bombowce');
    var podstawa_napis_bombowce = world.getObjectByName('podstawa_napis_bombowce');

    podstawa_bombowce.object.material.emissive = new THREE.Color(0x888888);
    podstawa_napis_bombowce.object.material.emissive = new THREE.Color(0x888888);

    podstawa_bombowce.addEvent(new TDU.Event('podstawa_bombowce_highlight', 'mousemove', function() {
        podstawa_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        podstawa_napis_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            podstawa_bombowce.object.material.emissive = new THREE.Color(0x888888);
            podstawa_napis_bombowce.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }, true));

    podstawa_bombowce.addEvent(new TDU.Event('bombowce_plan_ogolny', 'click', function() {
        world.worldState.setActiveState('bombowce');
    }, true));

    outliner.addObject(podstawa_bombowce, true);

    podstawa_napis_bombowce.addEvent(new TDU.Event('bombowce_plan_ogolny', 'click', function() {
        world.worldState.setActiveState('bombowce');
    }, true));

    podstawa_napis_bombowce.addEvent(new TDU.Event('podstawa_napis_bombowce_highlight', 'mousemove', function() {
        podstawa_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        podstawa_napis_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            podstawa_bombowce.object.material.emissive = new THREE.Color(0x888888);
            podstawa_napis_bombowce.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }, true));
}