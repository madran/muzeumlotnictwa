function objects_pulpit_bombowce() {
    var pulpit_bombowce = world.getObjectByName('pulpit_bombowce');
    var zeszyt_otwarty_pulpit_bombowce = world.getObjectByName('zeszyt_otwarty_pulpit_bombowce');

    pulpit_bombowce.object.material.emissive = new THREE.Color(0x888888);
    zeszyt_otwarty_pulpit_bombowce.object.material.emissive = new THREE.Color(0x888888);

    pulpit_bombowce.addEvent(new TDU.Event('pulpit_bombowce_najazd', 'click', function() {
        world.worldState.setActiveState('bombowce_pulpit');
    }));

    pulpit_bombowce.addEvent(new TDU.Event('pulpit_bombowce_highlight', 'mousemove', function() {
        pulpit_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        zeszyt_otwarty_pulpit_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            pulpit_bombowce.object.material.emissive = new THREE.Color(0x888888);
            zeszyt_otwarty_pulpit_bombowce.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    outliner.addObject(pulpit_bombowce);


    zeszyt_otwarty_pulpit_bombowce.addEvent(new TDU.Event('pulpit_bombowce_najazd', 'click', function() {
        world.worldState.setActiveState('bombowce_pulpit');
    }));

    zeszyt_otwarty_pulpit_bombowce.addEvent(new TDU.Event('zeszyt_otwarty_pulpit_bombowce_highlight', 'mousemove', function() {
        pulpit_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        zeszyt_otwarty_pulpit_bombowce.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            pulpit_bombowce.object.material.emissive = new THREE.Color(0x888888);
            zeszyt_otwarty_pulpit_bombowce.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));
}