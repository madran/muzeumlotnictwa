function objects_muzeum() {
    var podloga = world.getObjectByName('podloga');
    podloga.object.material.map.repeat.set(15, 15);

    var sciany = world.getObjectByName('muzeum');
    sciany.object.material.map.repeat.set(75, 75);


    var scena_mysliwce = world.getObjectByName('scena_mysliwce');
    scena_mysliwce.object.material.map.repeat.set(15, 15);
    scena_mysliwce.object.material.emissive = new THREE.Color(0x888888);

    var lina = world.getObjectByName('scena_mysliwiec_barierka_linka');
    lina.object.material.side = THREE.DoubleSide;
    lina.object.material.emissive = new THREE.Color(0x888888);
    
    
    var scena_mysliwce = world.getObjectByName('scena_bombowce');
    scena_mysliwce.object.material.map.repeat.set(15, 15);
    scena_mysliwce.object.material.emissive = new THREE.Color(0x888888);

    var scena_bombowiec_barierka_lina = world.getObjectByName('scena_bombowiec_barierka_lina');
    scena_bombowiec_barierka_lina.object.material.side = THREE.DoubleSide;
    scena_bombowiec_barierka_lina.object.material.emissive = new THREE.Color(0x888888);
    
    var scena_bombowce_krawedz_cien = world.getObjectByName('scena_bombowce_krawedz_cien');
    scena_bombowce_krawedz_cien.object.material.side = THREE.DoubleSide;
    scena_bombowce_krawedz_cien.object.material.emissive = new THREE.Color(0x444444);
    
    var scena_bombowce_krawedz = world.getObjectByName('scena_bombowce_krawedz');
    scena_bombowce_krawedz.object.material.side = THREE.DoubleSide;
    scena_bombowce_krawedz.object.material.emissive = new THREE.Color(0x444444);
    
    var projektor_mysliwce_metal = world.getObjectByName('projektor_mysliwce_metal');
    projektor_mysliwce_metal.object.material.emissive = new THREE.Color(0x444444);    
    
    var ekran_bombowce = world.getObjectByName('ekran_bombowce');
    ekran_bombowce.object.material.side = THREE.DoubleSide;
    ekran_bombowce.object.material.emissive = new THREE.Color(0x888888);
    
    var sufit = world.getObjectByName('sufit');
    sufit.object.material.side = THREE.DoubleSide;
    sufit.object.material.emissive = new THREE.Color(0x888888);
}