function objects_junkers() {
    var junkers_kadlub = world.getObjectByName('junkers_kadlub');
    var junkers_skrzydla = world.getObjectByName('junkers_skrzydla');
    var junkers_ogon = world.getObjectByName('junkers_ogon');
    var junkers_silniki = world.getObjectByName('junkers_silniki');
    var junkers_szklo = world.getObjectByName('junkers_szklo');
    

    junkers_kadlub.object.material.emissive = new THREE.Color(0x888888);
    junkers_skrzydla.object.material.emissive = new THREE.Color(0x888888);
    junkers_ogon.object.material.emissive = new THREE.Color(0x888888);
    junkers_silniki.object.material.emissive = new THREE.Color(0x888888);
    junkers_szklo.object.material.emissive = new THREE.Color(0x888888);

    outliner.addObject(junkers_kadlub, true);
    outliner.addObject(junkers_skrzydla, true);
    outliner.addObject(junkers_ogon, true);
    outliner.addObject(junkers_silniki, true);
    outliner.addObject(junkers_szklo, true);

    junkers_kadlub.addEvent(new TDU.Event('junkers_kadlub_highlight', 'mousemove', function() {
        junkers_kadlub.object.material.emissive = new THREE.Color(0xffffff);
        junkers_skrzydla.object.material.emissive = new THREE.Color(0xffffff);
        junkers_ogon.object.material.emissive = new THREE.Color(0xffffff);
        junkers_silniki.object.material.emissive = new THREE.Color(0xffffff);
        junkers_szklo.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            junkers_kadlub.object.material.emissive = new THREE.Color(0x888888);
            junkers_skrzydla.object.material.emissive = new THREE.Color(0x888888);
            junkers_ogon.object.material.emissive = new THREE.Color(0x888888);
            junkers_silniki.object.material.emissive = new THREE.Color(0x888888);
            junkers_szklo.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    junkers_szklo.addEvent(new TDU.Event('junkers_szklo_highlight', 'mousemove', function() {
        junkers_kadlub.object.material.emissive = new THREE.Color(0xffffff);
        junkers_skrzydla.object.material.emissive = new THREE.Color(0xffffff);
        junkers_ogon.object.material.emissive = new THREE.Color(0xffffff);
        junkers_silniki.object.material.emissive = new THREE.Color(0xffffff);
        junkers_szklo.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            junkers_kadlub.object.material.emissive = new THREE.Color(0x888888);
            junkers_skrzydla.object.material.emissive = new THREE.Color(0x888888);
            junkers_ogon.object.material.emissive = new THREE.Color(0x888888);
            junkers_silniki.object.material.emissive = new THREE.Color(0x888888);
            junkers_szklo.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    junkers_skrzydla.addEvent(new TDU.Event('junkers_skrzydla_highlight', 'mousemove', function() {
        junkers_kadlub.object.material.emissive = new THREE.Color(0xffffff);
        junkers_skrzydla.object.material.emissive = new THREE.Color(0xffffff);
        junkers_ogon.object.material.emissive = new THREE.Color(0xffffff);
        junkers_silniki.object.material.emissive = new THREE.Color(0xffffff);
        junkers_szklo.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            junkers_kadlub.object.material.emissive = new THREE.Color(0x888888);
            junkers_skrzydla.object.material.emissive = new THREE.Color(0x888888);
            junkers_ogon.object.material.emissive = new THREE.Color(0x888888);
            junkers_silniki.object.material.emissive = new THREE.Color(0x888888);
            junkers_szklo.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    junkers_ogon.addEvent(new TDU.Event('junkers_ogon_highlight', 'mousemove', function() {
        junkers_kadlub.object.material.emissive = new THREE.Color(0xffffff);
        junkers_skrzydla.object.material.emissive = new THREE.Color(0xffffff);
        junkers_ogon.object.material.emissive = new THREE.Color(0xffffff);
        junkers_silniki.object.material.emissive = new THREE.Color(0xffffff);
        junkers_szklo.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            junkers_kadlub.object.material.emissive = new THREE.Color(0x888888);
            junkers_skrzydla.object.material.emissive = new THREE.Color(0x888888);
            junkers_ogon.object.material.emissive = new THREE.Color(0x888888);
            junkers_silniki.object.material.emissive = new THREE.Color(0x888888);
            junkers_szklo.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    junkers_silniki.addEvent(new TDU.Event('junkers_silniki_highlight', 'mousemove', function() {
        junkers_kadlub.object.material.emissive = new THREE.Color(0xffffff);
        junkers_skrzydla.object.material.emissive = new THREE.Color(0xffffff);
        junkers_ogon.object.material.emissive = new THREE.Color(0xffffff);
        junkers_silniki.object.material.emissive = new THREE.Color(0xffffff);
        junkers_szklo.object.material.emissive = new THREE.Color(0xffffff);
        document.body.style.cursor = "pointer";
        return function() {
            junkers_kadlub.object.material.emissive = new THREE.Color(0x888888);
            junkers_skrzydla.object.material.emissive = new THREE.Color(0x888888);
            junkers_ogon.object.material.emissive = new THREE.Color(0x888888);
            junkers_silniki.object.material.emissive = new THREE.Color(0x888888);
            junkers_szklo.object.material.emissive = new THREE.Color(0x888888);
            document.body.style.cursor = "default";
        }
    }));

    junkers_kadlub.addEvent(new TDU.Event('junkers_kadlub_zoom', 'click', function() {
        world.worldState.setActiveState('bombowce_zoom');
    }));

    junkers_szklo.addEvent(new TDU.Event('junkers_szklo_zoom', 'click', function() {
        world.worldState.setActiveState('bombowce_zoom');
    }));

    junkers_skrzydla.addEvent(new TDU.Event('junkers_skrzydla_zoom', 'click', function() {
        world.worldState.setActiveState('bombowce_zoom');
    }));

    junkers_ogon.addEvent(new TDU.Event('junkers_ogon_zoom', 'click', function() {
        world.worldState.setActiveState('bombowce_zoom');
    }));

    junkers_silniki.addEvent(new TDU.Event('junkers_silniki_zoom', 'click', function() {
        world.worldState.setActiveState('bombowce_zoom');
    }));
}